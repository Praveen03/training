
-- 5.Insert Records into Table

INSERT INTO `database`.`student` (`first_name`, `surname`, `class`, `roll_no`) 
VALUES ('Praveen', 'M', '10', '100');

INSERT INTO `database`.`student` (`first_name`, `surname`, `class`, `roll_no`) 
VALUES ('Mahesh', 'R', '11', '101');

INSERT INTO `database`.`student` (`first_name`, `surname`, `class`, `roll_no`) 
VALUES ('Rahul', 'B', '12', '102');
