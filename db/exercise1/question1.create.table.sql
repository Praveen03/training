

-- 1.Create Database\table and Rename\Drop Table


-- CREATE DATABASE

CREATE SCHEMA `database` ;


-- CREATE TABLE 

CREATE TABLE `database`.`student` 
(
  `roll_no` INT NULL
);


-- RENAME TABLE

ALTER TABLE `database`.`student` 
RENAME TO  `database`.`employee` ;


-- DROP TABLE

DROP TABLE `database`.`employee`;