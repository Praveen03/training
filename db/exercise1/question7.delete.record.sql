
-- 7.Delete Records from the table with/without WHERE

-- DELETE RECORD using WHERE

DELETE FROM `database`.`student` WHERE (`roll_no` = '102');


-- DELETE RECORD without using WHERE

DELETE FROM `database`.`student` ;
