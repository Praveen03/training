
-- 6.Update Field value on the Fields with/without WHERE

-- UPDATE field using WHERE

UPDATE `database`.`student` 
SET `class` = '11' 
WHERE (`roll_no` = '100');

-- UPDATE field without using WHERE

UPDATE `database`.`student` 
SET `class` = '10' ;