

-- 3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, DROP KEY, INDEX


-- NOT NULL

ALTER TABLE `database`.`student` 
CHANGE COLUMN `name` `name` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `roll_no` `roll_no` INT(11) NOT NULL ;


-- DEFAULT

ALTER TABLE `database`.`student` 
CHANGE COLUMN `class` `class` INT(11) NOT NULL DEFAULT '8' ;


-- CHECK

ALTER TABLE `database`.`student`
ADD CHECK (class>=10);


-- PRIMARY KEY

ALTER TABLE `database`.`student` 
ADD PRIMARY KEY (`roll_no`);


-- FOREIGN KEY

ALTER TABLE `database`.`student` 
ADD CONSTRAINT `fk_roll_no`
  FOREIGN KEY (`roll_no`)
  REFERENCES `database`.`school` (`roll_no`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

-- DROP KEY

ALTER TABLE `database`.`student` 
DROP FOREIGN KEY `fk_roll_no`;


-- INDEX 

ALTER TABLE `database`.`student` 
ADD INDEX `index_surname` (`surname` ASC) ;
