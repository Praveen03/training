
-- 13.At-least use 3 tables for Join Union 2 Different table with same Column name (use Distinct,ALL)

-- Union

SELECT product_name,product_quantity 
FROM `database`.product  
UNION  
SELECT orderdetails.product_name,quantity 
FROM `database`.orderdetails   ; 


-- DISTINCT 

SELECT DISTINCT product.product_quantity 
FROM `database`.product;  

-- ALL

SELECT product_name 
FROM `database`.product
WHERE product_id = 
ALL(
SELECT order_id 
FROM `database`.orderdetails 
WHERE quantity = 10
);
