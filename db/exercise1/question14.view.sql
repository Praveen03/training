
-- 12.Create VIEW for your Join Query and select values from VIEW

-- CREATE VIEW

CREATE VIEW view
AS SELECT orderdetails.order_id , customer.cust_name , product.product_name, product.price
FROM `database`.product, `database`.customer, `database`.orderdetails
WHERE customer.order_id = orderdetails.order_id
AND orderdetails.product_id = product.product_id;

-- VIEW

SELECT * FROM view;