

-- 2.Alter Table with Add new Column and Modify\Rename\Drop column


-- ADD NEW COLUMN

ALTER TABLE `database`.`student` 
ADD COLUMN `class` INT NULL AFTER `roll_no`,
ADD COLUMN `section` VARCHAR(45) NULL AFTER `class`;


-- Rename column

ALTER TABLE `database`.`student` 
CHANGE COLUMN `section` `name` VARCHAR(45) NULL ;


-- DROP COLUMN

ALTER TABLE `database`.`student` 
DROP COLUMN `name`;
