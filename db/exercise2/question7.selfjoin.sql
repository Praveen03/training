
-- 7. Prepare an example for self-join

 
-- self-join

-- A self join is a regular join, but the table is joined with itself using aliases.

-- Each employee has manager in the table

-- Each manager is employee  in the table

-- We can join the table using self-join 

SELECT
a.emp_id AS "Emp_ID",a.first_name AS "Employee_Name",
b.mag_id AS "Manager_ID",b.fist_name AS "Manager_Name"
FROM `database`.employee a, `database`.employee b
WHERE a.emp_id = b.mag_id;
