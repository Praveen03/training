
--  11. Write a query to get employee names and their respective department name

SELECT empl.first_name,empl.surname,dept.department_name  
FROM  database.employee empl , database.department dept  
WHERE  empl.department_number = dept.department_number;
