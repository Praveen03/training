
-- 2. Write a query to insert at least 5 employees for each department

INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('101', 'Mahesh', 'R', '2001/02/09', '2015/05/12', '1000000','1');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('102', 'Praveen', 'M', '2001/03/17', '2016/04/19', '900000','1');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('103', 'Rahul', 'B', '2001-06-02', '2016-05-19', '800000','1');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('104', 'Venkatachalam', 'K', '1999-03-02', '2015-05-05', '1000000','1');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('105', 'Arun', 'K', '2000-06-06', '2016-05-12', '1200000','1');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`)
VALUES ('106', 'Raja', 'R', '2001-02-09', '2015-05-12', '100000', '2');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('107', 'Ram', 'T', '2001-03-17', '2016-05-19', '12000', '2');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`)
VALUES ('108', 'Manoj', 'F', '2001-02-09', '2016-04-19', '133000', '2');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('109', 'Pradheep', 'H', '2001-02-09', '2015-05-12', '140000', '2');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('110', 'Sarvesh', 'K', '2001-03-17', '2016-05-19', '150000', '2');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('111', 'Pandian', 'L', '1999-03-02', '2016-04-19', '1800000', '3');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('112', 'Nishanth', 'V', '2000-06-06', '2015-05-12', '190000', '3');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('113', 'Prasath', 'B', '1999-03-02', '2016-05-19', '150000', '3');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('114', 'Naveen', 'E', '2001-02-09', '2016-04-19', '190000', '3');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('115', 'Nandha Kumar', 'T', '2001-03-17', '2015-05-12', '1500000', '3');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('116', 'Kishore', 'N', '1999-03-02', '2016-05-19', '150000', '4');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('117', 'Mervine', 'D', '1999-03-02', '2016-04-19', '200000', '4');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('118', 'Kiran', 'A', '2001-02-09', '2015-05-12', '250000', '4');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('119', 'Shakthi', 'S', '2001-03-17', '2016-05-19', '170000', '4');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('120', 'Rajesh', 'C', '2000-06-06', '2016-04-19', '189000', '4');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('121', 'Thaga', 'V', '1999-03-02', '2016-05-19', '155000', '5');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('122', 'Mohammed', 'K', '2001-02-09', '2016-04-19', '160000', '5');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('123', 'Lokesh', 'L', '2001-03-17', '2015-05-12', '1600000', '5');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('124', 'Karthi', 'E', '1999-03-02', '2016-04-19', '301300', '5');
INSERT INTO `database`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_number`) 
VALUES ('125', 'Mukilan', 'R', '2001-03-17', '2015-05-12', '453000', '5');
