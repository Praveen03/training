
-- 8. Write a query to list out employees from the same area, and from the same department


-- same area

SELECT first_name,area
FROM `database`.employee
ORDER BY area;



-- same department

SELECT first_name,department_number
FROM `database`.employee
ORDER BY department_number;

