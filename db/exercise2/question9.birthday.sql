
-- 9. Write a query to find out employee birthday falls on that day

SELECT first_name
FROM `database`.employee
WHERE date_format(dob,'%d-%m') = date_format(curdate(),'%d-%m'); 