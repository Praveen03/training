
-- 10. Display below result in one SQL run

-- a) Shows students details who scored above 8 GPA for the given semester


    SELECT 
         s.id 
        ,s.roll_number
        ,s.name AS student_name
        ,s.gender
        ,c.code AS college_code
        ,c.name AS college_name
        ,sr.semester
        ,sr.grade
        ,sr.gpa
    FROM 
        university u
       ,college c
       ,student s
       ,semester_result sr
    WHERE 
            u.univ_code = c.univ_code
        AND s.college_id = c.id
        AND sr.stud_id = s.id
        AND sr.gpa>8;        

-- b) Shows students details who scored above 5 GPA for the given semester


    SELECT 
         s.id 
        ,s.roll_number
        ,s.name AS student_name
        ,s.gender
        ,c.code AS college_code
        ,c.name AS college_name
        ,sr.semester
        ,sr.grade
        ,sr.gpa
    FROM 
        university u
       ,college c
       ,student s
       ,semester_result sr
    WHERE 
            u.univ_code = c.univ_code
        AND s.college_id = c.id
        AND sr.stud_id = s.id
        AND sr.gpa>5;        