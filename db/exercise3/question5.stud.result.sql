
-- 5. List Students details along with their GRADE,CREDIT and GPA details from all universities. 
-- Result should be sorted by college_name and semester. 
-- Apply paging also.



    SELECT 
         stud.id AS stud_id    
        ,stud.roll_number
        ,stud.name
        ,stud.dob
        ,stud.gender
        ,stud.phone
        ,clg.name AS COLLEGE_NAME
        ,sr.semester
        ,sr.grade
        ,sr.credits
    FROM 
         student stud
        ,university u
        ,semester_result sr
        ,college clg
    WHERE
            u.univ_code = clg.univ_code
        AND stud.id = sr.stud_id
        AND clg.id = stud.college_id
    ORDER BY 
          sr.semester
         ,clg.name;