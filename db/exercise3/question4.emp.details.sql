
-- 4. List Employees details from a particular university along with their college and department details. 
-- Details should be sorted by rank and college name.


     SELECT 
         emp.id AS emp_id
        ,emp.name AS emp_name
        ,emp.dob
        ,emp.email
        ,emp.phone
        ,des.name AS designation_name
        ,des.rank AS designation_rank
        ,clg.name AS college_name            
        ,u.univ_code
        ,u.university_name
    FROM
         employee emp
        ,college clg
        ,university u
        ,designation des
    WHERE 
            clg.id = emp.college_id
        AND clg.univ_code = u.univ_code
        AND des.id = emp.desig_id
        AND u.university_name = 'KS University'   
    ORDER BY 
         clg.name
        ,des.rank
