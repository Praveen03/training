
-- 7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who has done the payment. 
-- Entries should be updated based on student ROLL_NUMBER.


-- a) Single Update - Update one student entry

    UPDATE semester_fee
    SET 
        amount = '500'
    WHERE ( '501' = semester_fee.stud_id);
    

-- b) Bulk Update - Update multiple students entries

    
    UPDATE semester_fee
    SET 
        amount = '500'   
    WHERE  semester_fee.stud_id IN ('501','502','503','504');

-- c) PAID_STATUS value as ‘Paid’

    UPDATE semester_fee
    SET 
        paid_status = 'paid'    
    WHERE  semester_fee.stud_id IN ('501','502','503','504');


-- d) PAID_YEAR value as ‘year format’

    UPDATE semester_fee
    SET 
        paid_year = '2020'    
    WHERE  semester_fee.stud_id IN ('501','502','503','504');
