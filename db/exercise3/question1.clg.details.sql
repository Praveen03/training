
-- 1. Select College details which are having IT / CSC departments across all the universities. 
-- Result should have below details;
-- (Assume that one of the Designation name is ‘HOD’ in Designation table)

-- CODE, COLLEGE_NAME, UNIVERSITY_NAME, CITY, STATE, YEAR_OPENED, DEPTARTMENT_NAME, HOD_NAME

    
    SELECT  
         college.code 
        ,college.name 
        ,university.university_name 
        ,college.city 
        ,college.state 
        ,college.year_opened 
        ,department.dept_name 
        ,employee.name AS HOD_NAME
    FROM 
		 department
        ,employee
        ,designation
		,college 
		 INNER JOIN university ON college.univ_code = university.univ_code 
		 INNER JOIN college_department ON college_department.college_id = college.id
    WHERE 
            employee.cdept_id = college_department.cdept_id
        AND employee.desig_id = designation.id
        AND college_department.udept_code = department.dept_code
        AND department.dept_name IN ("IT", "CSE") ;