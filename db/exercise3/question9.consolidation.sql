
-- 9. Show consolidated result for the following scenarios.

-- a) collected and uncollected semester fees amount per semester for each college under an university. 
-- Result should be filtered based on given year.

-- COLLECTED_FEES

    SELECT 
         sf.semester
        ,SUM(amount) AS collected_fees 
        ,u.university_name 
        ,sf.paid_year
    FROM 
         semester_fee sf
        ,university u
        ,student s
        ,college c
    WHERE
            sf.stud_id = s.id
        AND s.college_id = c.id
        AND c.univ_code = u.univ_code
        AND sf.stud_id = s.id
        AND u.university_name = 'Bharathiyar University'
        AND paid_year = '2020'
        AND semester = '1'
        AND paid_status = 'paid';
        
        
-- UNCOLLECTED_FEES
   
   SELECT 
         sf.semester
        ,SUM(amount) AS uncollected_fees 
        ,u.university_name 
        ,sf.paid_year
    FROM 
         semester_fee sf
        ,university u
        ,student stud
        ,college clg
    WHERE
            sf.stud_id = stud.id
        AND stud.college_id = clg.id
        AND clg.univ_code = u.univ_code
        AND sf.stud_id = stud.id
        AND u.university_name = 'Bharathiyar University'
        AND paid_year = '2020'
        AND semester = '2'
        AND paid_status = 'unpaid';
        

-- b) Collected semester fees amount for each university for the given year


    SELECT  
    DISTINCT
         u.university_name 
        ,sf.semester
        ,sf.paid_year
        ,sf.amount
    FROM 
         college c
        ,university u
        ,semester_fee sf
        ,student s
        ,college_department cd
        ,department d
    WHERE 
            c.univ_code = u.univ_code 
        AND sf.stud_id = s.id
        AND s.college_id = c.id
        AND semester = '2' ;

     