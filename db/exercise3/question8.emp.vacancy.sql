
-- 8. Find employee vacancy position in all the departments from all the colleges and across the universities. 
-- Result should be populated with following information.. 

-- Designation, rank, college, department and university details.

    SELECT 
		 designation.name AS designation
		,designation.rank 
		,college.name AS college_name
		,department.dept_name
		,university.university_name
   FROM
		 department 
        ,university
        ,designation
	    ,employee 
         INNER JOIN college_department ON  employee.cdept_id = college_department.cdept_id
         INNER JOIN college ON employee.college_id = college.id       
   WHERE 
         employee.name is NULL
	 AND employee.desig_id = designation.id
     AND college_department.udept_code = department.dept_code
     AND college.univ_code = university.univ_code 