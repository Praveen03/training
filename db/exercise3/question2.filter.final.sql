
-- Select final year students(Assume that universities has Engineering Depts only) 
-- Details who are studying under a particular university and selected cities alone.

-- ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS, COLLEGE_NAME, DEPARTMENT_NAME


    SELECT 
         student.roll_number
        ,student.name
        ,student.gender
        ,student.dob
        ,student.email
        ,student.phone
        ,student.address
        ,college.name AS college_name
        ,department.dept_name AS department_name 
    FROM 
         department 
        ,university 
		,student 
        RIGHT JOIN college ON student.college_id = college.id
        RIGHT JOIN college_department ON student.cdept_id = college_department.cdept_id
    WHERE 
            university.univ_code = college.univ_code
        AND college_department.udept_code = department.dept_code
        AND university.university_name = 'KS University'
        AND student.academic_year = "2022"
        AND college.city = 'Pollachi' ;