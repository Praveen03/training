

-- Select students details who are studying under a particular university and selected cities alone. Fetch 20 records for every run.

-- ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS,COLLEGE_NAME, DEPARTMENT_NAME, HOD_NAME


   SELECT  
         student.roll_number
        ,student.name
        ,student.gender
        ,student.dob
        ,student.email
        ,student.phone
        ,student.address
        ,college.name AS COLLEGE_NAME
        ,department.dept_name
        ,employee.name AS HOD_NAME
    FROM 
         university 
        ,employee 
        RIGHT JOIN college ON employee.college_id = college.id
        RIGHT JOIN designation ON employee.desig_id = designation.id
        ,college_department
        LEFT JOIN student ON  student.cdept_id = college_department.cdept_id
        LEFT JOIN department ON college_department.udept_code = department.dept_code
    WHERE 
            college.univ_code = university.univ_code 
		AND student.college_id = college.id
        AND university.university_name = 'KS University'
        AND college.city = 'Pollachi' ;