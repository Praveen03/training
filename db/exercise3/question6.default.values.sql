
-- 6. Create new entries in SEMESTER_FEE table for each student from all the colleges and across all the universities.
-- These entries should be created whenever new semester starts.


-- Each entry should have below default values;



--    a) AMOUNT - Semester fees

     ALTER TABLE `university`.`semester_fee` CHANGE COLUMN `amount` `amount` VARCHAR(20) NULL DEFAULT 'Semester fees' ;
    
   
--    b) PAID_YEAR - Null

     ALTER TABLE `university`.`semester_fee` CHANGE COLUMN `paid_year` `paid_year` YEAR(4) NULL DEFAULT NULL ;
 

--    c) PAID_STATUS - Unpaid

     ALTER TABLE `university`.`semester_fee`  CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'Unpaid' ;