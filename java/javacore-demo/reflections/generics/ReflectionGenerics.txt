1.Will the following class compile? If not, why?
	
	public final class Algorithm {
	    public static <T> T max(T x, T y) {
	        return x > y ? x : y;
	    }
	}
	
	Answer: No. The greater than (>) operator applies only to primitive numeric types.

2.If the compiler erases all type parameters at compile time, why should you use generics?

Answer: 
	You should use generics because:
	The Java compiler enforces tighter type checks on generic code at compile time.
	Generics support programming types as parameters.
	Generics enable you to implement generic algorithms.