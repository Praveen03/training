/*
 Question :
 	3) Delete a file using visitFile() method.

WBS :

1.Requirements:
      Program to delete a file using visitFile() method.
    
2.Entities:
      DeleteVisitFileMethod
    
4.Jobs to be done:
    1.Create file SimpleFileVisitor class with generic type of Path 
    2.Invoke the walkFileTree method it invoke
       2.1)Check file get file Name using getFileName,convert to String using toString , contains method to file exists or not.
       2.2)Invoke Files class delete method to delete file
       2.3)Return the FileVisitResult class CONTINUE to stop process.

Pseudo Code:

public class DeleteVisitFileMethod {
	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				
				if(file.getFileName().toString().contains("deletefile.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;
				
			}
		};
		Files.walkFileTree(Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/"), fv);
	}
}

*/

package com.kpriet.java.nio;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteVisitFileMethod {
	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				
				if(file.getFileName().toString().contains("deletefile.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;	
			}
		};
		Files.walkFileTree(Paths.get("C:\\1Dev\\eclipes\\javacore-demo\\src\\com\\kpriet\\java\\nio"), fv);
	}
}
