/*
 Question :
 	Demonstrate linked hash set to array() method in java
 	
 Requirement:
    Demonstrate linked hash set to array() method in java
    
 Entity:
    LinkedHashSetDemo
    
 Jobs to be done:
   1.Create a class and declaring main.
   2.Inside the main creating a empty linked list and adding elements in hash set using add() method.
   3.Display the linked list element.
   4.If the linked list element is convert to array() elements.
   5.Display the elements.
   
 Pseudo Code :
 
 public class LinkedHashSetDemo {
	
	public static void main(String args[]) { 
	
		LinkedHashSet<String> set = new LinkedHashSet<String>(); 
		// add the elements to the linkedhashset
		// display the elements in the linkedhashset
		// create array
		// convert linked list element to array.
		// display the element
	}	
 }
 
 */


package com.kpriet.java.setmap;

import java.util.*; 

public class LinkedHashSetDemo {
	
	public static void main(String args[]) { 
		
		LinkedHashSet<String> set = new LinkedHashSet<String>(); 
 
		set.add("Redmi"); 
		set.add("Realme"); 
		set.add("Vivo"); 
		set.add("Samsung"); 
		set.add("Oppo");
 
		System.out.println("LinkedHashSet: " + set); 
		
		String[] arr = new String[5]; 
		arr = set.toArray(arr); 

		System.out.print("\nArray is : "); 
		
		for(int j = 0; j < arr.length; j++) {
			System.out.print(arr[j]+" "); 
	    }
		
	}	
	
} 
