/*
 Question :
 	 Java program to demonstrate insertions and string buffer in tree set
 	 
 Requirement:
     Java program to demonstrate insertions and string buffer in tree set
    
 Entity:
     StringBufferTreeSet

 Method Signature:
    public int compare(StringBuffer s1, StringBuffer s2) 

 Jobs to be done:
	 1.Create a class and declaring main method.
     2.By implementing Comparator interface
     3.By converting StringBuffer to String using StringBuffer.toString() method
	 4.Inside the main creating set called str and adding elements in tree set using add() method.
	 5.Display the element.
   
 Pseudo Code:
 
   public class StringBufferTreeSet implements Comparator<StringBuffer> {
   		public int compare(StringBuffer s1, StringBuffer s2) { 
		 	 return s1.toString().compareTo(s2.toString()); 
	     } 
   	  public static void main(String[] args) {
   	 	 // converting StringBuffer to String using StringBuffer.toString() method
   	 	 // Display the elements
   	  }
   	}
 */


package com.kpriet.java.setmap;
 
import java.util.Comparator; 
import java.util.Set; 
import java.util.TreeSet; 

public class StringBufferTreeSet implements Comparator<StringBuffer> { 
	
	public int compare(StringBuffer s1, StringBuffer s2) 
	{ 
		return s1.toString().compareTo(s2.toString()); 
	} 

	public static void main(String[] args) {
		
		Set<StringBuffer> str = new TreeSet<>(new StringBufferTreeSet()); 
		
		str.add(new StringBuffer("Apple")); 
		str.add(new StringBuffer("Orange")); 
		str.add(new StringBuffer("Mango")); 
		
		System.out.println(str); 
	} 
	
} 
