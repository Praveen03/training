/*
 Question :
 	1.java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 	
 WBS :	
 
 Requirement:
 	1.java program to demonstrate adding elements, displaying, removing, and iterating in hash set
  
 Entity:
    HashSetElement
 
 Jobs to be done:
   1.Create a class and declaring main.
   2.Inside the main creating set called hash and adding elements in hash set using add() method.
   3.Check the element  
     3.1)If the element is placed then display the element
   4.If the element is remove then print the other elements 
   5.Display all the set elements using Iterator interface
   
  Pseudo Code :
   
   public class HashSetElement {		  
       public static void main(String[] args) {
	       HashMap<Integer, String> hash = new HashMap<Integer, String>();
	 		// adding the elements in hashmap
	 		// remove element in hashmap
	 		// iterating element in hashmap
	 		// displaing all the elements
       }
   }
   
*/


package com.kpriet.java.setmap;

import java.util.*;

public class HashSetElement {		  
	   
	public static void main(String[] args) {
		 
	     HashSet<String> hash = new HashSet<String>(); 
	        
	     //Adding the elements
         hash.add("chrome"); 
	     hash.add("telegram"); 
	     hash.add("youtube"); 
	     hash.add("dream 11"); 
	        
	     // Displaying the HashSet 
	     System.out.println(hash); 
	     System.out.println("--- List contains dream 11 ---");
	     System.out.println(hash.contains("dream 11")); 
	  
	     // Removing items from HashSet using remove() 
	     hash.remove("youtube"); 
	     System.out.println("--- remove() method ---"); 
	     System.out.println(hash);
	     
         // Iterating over hash set items 
	     System.out.println("--- Iterating list ---"); 
	     Iterator<String> i = hash.iterator(); 
         while(i.hasNext()) { 
             System.out.println(i.next()); 
	     } 
	}

}





