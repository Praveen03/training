/*
 Question :
 	5.Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ).
      If possible try remaining methods.

 WBS :

 Requirement:
    Demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ).
    If possible try remaining methods.
    
 Entity:
     HashMapDemo
 
 Jobs to be done:
    1.Create a HashMap with type integer and String. 
    2.Check the methods    
        2.1)Add elements in hash object using put() method.
        2.2)Remove the element using remove() method
        2.3)Replace the another value using replace() method.
        2.4)Print boolean value true or false using containsValue() method.
    3.Display the hash.
    
 Pseudo code :
 
 public class HashMapDemo {
 	public static void main(String[] args) {
 		HashMap<Integer, String> hash = new HashMap<Integer, String>();
 		// adding the elements in hashmap
 		// remove element in hashmap
 		// replace the element in hashmap
 		// print value true or false using containsValue() method 
 	}
 }
           
 */
package com.kpriet.java.setmap;

import java.util.HashMap;

public class HashMapDemo {

	public static void main(String[] args) {
		
		HashMap<Integer, String> hash = new HashMap<Integer, String>(); 

		hash.put(10, "Java"); 
		hash.put(15, "Python"); 
		hash.put(20, "Javascript"); 
		hash.put(25, "Php"); 
		hash.put(30, "Ruby"); 

		System.out.println(hash); 

		// Removing the existing key mapping 
		String returned_value = (String)hash.remove(20); 

		// Verifying the returned value 
		System.out.println("Returned value is: "+ returned_value); 

		// Display in the new map 
		System.out.println("New map is: "+ hash);  
		
		//Contains Value
		System.out.println(hash.containsValue("Php"));
		
		//replace the value
		System.out.println(hash.replace(25, "OOPS"));
		
		//Display the values
		System.out.println(hash.values());
		
		//Display the keys
		System.out.println(hash.keySet());

	}

}
