/* 
Question :
	
	Create an array list with 7 elements, and create an empty linked list add all elements of 
 the array list to linked list ,traverse the elements and display the result
 
WBS :
 
1.Requirements:
   Create an array list with 7 elements, and create an empty linked list add all elements of 
the array list to linked list ,traverse the elements and display the result
 
2.Entities
    ArrayLinkedList

3.Jobs to be done
    1. Create a class ArrayLinkedList and main method.
    2. Create array list name apps1.
    3. Add 7 values in the list.
    5. Create linked list name apps2.
    6. Add all values in array list to linked list.
    7. Using iterator print all values
    
Pseudo Code :
	public class ArrayLinkedList {
		public static void main(String [] args) {
			List<String> apps1 = new ArrayList<String>();
			// add 7 value to the list
			List<String> apps2 = new LinkedList<String>();
			// Add all values in array list to linked list
			// Using iterator print all values
		}
	}

*/
package com.kpriet.java.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class ArrayLinkedList {
	public static void main(String [] args) {
		List<String> apps1 = new ArrayList<String>();
		 apps1.add("Youtube");
	     apps1.add("Amazon");
	     apps1.add("Flipkart");
	     apps1.add("Instagram");
	     apps1.add("Facebook");
	     apps1.add("MX Player");
	     apps1.add("Chrome");
		System.out.println("Elements in the array List : "+apps1);
		List<String> apps2 = new LinkedList<String>();
        while(apps1.size() > 0) {
        	apps2.add(apps1.remove(0));
            }
		System.out.println("Elements in the Linked List : "+apps2);
        Iterator<String> iter = apps2.listIterator(4); 
        System.out.print("\nTraverse the elements using Iterator : "); 
        while(iter.hasNext()){ 
           System.out.print(iter.next()+" "); 
        } 
	}

}
