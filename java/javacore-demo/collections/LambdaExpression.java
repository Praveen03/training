/*
Question :

  Addition,Substraction,Multiplication and Division concepts are achieved using 
Lambda expression and functional interface.

WBS 

1.Requirements:
   Addition,Substraction,Multiplication and Division concepts are achieved using 
Lambda expression and functional interface.

2.Entities
    LambdaExpression
    Arthmetic - Interface 
    
3.Method Signature
    int operation(int a, int b)
    
4.Jobs to be done
    1. Create a class LambdaExpDemo and main method.
    2. Create a interface Arithmetic.
    3. And using method int operation(int a, int b).
    4. Inside that Addition,Substraction,Multiplication and Division concepts are 
achieved using Lambda expression 
    5. Display the values

pseudo Code 
	interface Arithmetic {
		int operation(int a, int b);
	}
	public class LambdaExpression {
		public static void main(String[] args) {
			Arithmetic addition = (int a, int b) -> (a + b);
			Arithmetic subtraction = (int a, int b) -> (a - b);
			Arithmetic multiplication = (int a, int b) -> (a * b);
			Arithmetic division = (int a, int b) -> (a / b);
			// print all the result.
		}
	}

*/

package com.kpriet.java.collections;

interface Arithmetic {
	int operation(int a, int b);
}
public class LambdaExpression {
	
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 6));
		
		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(5, 3));
		
		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(4, 6));
		
		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 6));
	}
}
