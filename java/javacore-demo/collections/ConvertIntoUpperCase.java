/*
Question :
 
   8 districts are shown below
  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy to be converted to UPPERCASE.
  
1.Requirements:
    8 districts are shown below
  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy to be converted to UPPERCASE.
 
2.Entities
    ConvertIntoUpperCase
    
3.Jobs to be done
    1. Create a class ConvertIntoUpperCase and main method.
    2. Create a list districts.
    3. Add given 8 districts in the list.
    4. Using toUpperCase() method convert the list into upper case.
    5. Print the list using foreach.
    
Pseudo Code
	public class ConvertIntoUpperCase {
		public static void main(String[] args) {
			List<String> districts = new ArrayList<String>();
			// add given 8 districts in the list.
			// using toUpperCase() method convert the list into upper case
			for(String display : districts) {
	     		System.out.print(display.toUpperCase()+" ");
	    	}
		}
	}

*/
package com.kpriet.java.collections;

import java.util.ArrayList;
import java.util.List;

public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> districts = new ArrayList<String>();
	districts.add("Madurai"); 	//Add 8 values in the list
	districts.add("Coimbatore");
	districts.add("Theni");
	districts.add("Chennai");
	districts.add("Karur");
	districts.add("Salem");
	districts.add("Erode");
	districts.add("Trichy ");
	for(String display : districts) {
	     System.out.print(display.toUpperCase()+" ");
	    }
    }
}
