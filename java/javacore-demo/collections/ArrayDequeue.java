/* 
Question :
	Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast()
methods to store and retrieve elements in ArrayDequeue.

Requirements:
    Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast()
  methods to store and retrieve elements in ArrayDequeue.

Entities
    ArrayDequeue
    
Jobs to be done
    1. Create a class name ArrayDequeue and main method.
    2. Create a array list name apps.
    4. Add values using addFirst(),addLast(),add methods in the dequeue.
    5. print peek values using peekFirst(),peekLast() methods in the dequeue.
    6. Remove element using pollFirst(),pollLast(),removeFirst(),removeLast() methods in the dequeue
    7. Print the dequeue
Pseudo code
 	public class ArrayDequeue {
		public static void main(String [] args) {
			Deque<String> apps = new LinkedList<String>();
			// add the elements to deque using addFirst() and addLast()
			// using the peekFirst() and peekLast() method and print it
			// using the pollFirst() and pollLast() method and print it
			// using the removeFirst() and removeLast() method and print it
			// print the final result.
		}
	}
 */
package com.kpriet.java.collections;

import java.util.LinkedList;
import java.util.Deque;
public class ArrayDequeue {
	public static void main(String [] args) {
		Deque<String> apps = new LinkedList<String>();
		apps.addFirst("Youtube");
        apps.addFirst("Amazon");
        apps.addFirst("Flipkart");
        apps.addFirst("Instagram");
        apps.addLast("Facebook");
        apps.addLast("MX Player");
        apps.addLast("Chrome");
        System.out.println(apps);
		System.out.println("Peek First Value: "+apps.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+apps.peekLast()); //peek last value
		System.out.println("pollFirst() : "+apps.pollFirst()+"\n"+"pollLast() : "+apps.pollLast());
		System.out.println(apps);
		System.out.println("removeFirst() : "+apps.removeFirst());  //remove first value
		System.out.println("removeLast() : "+apps.removeLast());  //remove last value
		System.out.println(apps);
	}

}
