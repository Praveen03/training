/*
Question
	5.Add and remove the elements in stack.

WBS 
 
Requirements:
    Add and remove the elements in stack.
    
Entities
    AddRemoveStack
   
Jobs to be done
    1. Create a class AddRemoveStack
    3. Create a stack name mobile.
    4. Add values on the stack.
    5. And then pop top element and print it.
    
 Pseudo code :
    public class AddRemoveStack {
    	public static void main(String [] args) {
    	 	Stack<String> mobile = new Stack<String>(); 
    	 	// use push() method to add value
    	 	// use pop() method to remove value
    	 	// print the result
    	}
    }
*/

package com.kpriet.java.collections;

import java.util.Stack;

public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> mobile = new Stack<String>(); 
        mobile.push("Apple"); 
        mobile.push("Vivo");
        mobile.push("Realme");
        mobile.push("Redmi"); 
        mobile.pop();  
        System.out.println(mobile);
    }
}
