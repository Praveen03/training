/*
Question :
  LIST CONTAINS 10 STUDENT NAMES
     krishnan, abishek, arun,vignesh, kiruthiga, murugan, adhithya, balaji, vicky, priya 
     and display only names starting with 'A'.
    
1.Requirements:
   LIST CONTAINS 10 STUDENT NAMES
     krishnan, abishek, arun,vignesh, kiruthiga, murugan, adhithya, balaji, vicky, priya 
     and display only names starting with 'A'.
2.Entities
    StudentList

3.Jobs to be done
    1. Create a class StudentList and main method.
    2. Create a list as student.
    3. Add 10 student on the list using add() method.
    4. Display only names starting with 'A'
    
Pseudo Code :
	public class StudentList {
		public static void main(String[] args) {
			List<String> student = new ArrayList<String>();
			// add 10 student on the list
			// print only names starting with 'A' using startWith() method
		}
    }
*/
package com.kpriet.java.collections;

import java.util.ArrayList;
import java.util.List;

public class StudentList {
	public static void main(String[] args) {
		
		List<String> student = new ArrayList<String>();
		student.add("krishnan"); 	//Add 10 values in the list
		student.add("abishek");
		student.add("arun");
		student.add("vignesh");
		student.add("kiruthiga");
		student.add("murugan");
		student.add("adhithya");
		student.add("balaji");
		student.add("vicky");
		student.add("priya");
		for(String display : student) {
		    if(display.startsWith("a")) 
		    	System.out.print(display+" ");
		}
	}
 }

