/*Generics-Class literals:
1. Write a program to demonstrate generics - class objects as type literals.
2. Write a program to demonstrate generics - for loop, for list, set and map. 

WBS :

1.Requirement:
    - Write a program to demonstrate generics - class objects as type literals.
    - Write a program to demonstrate generics - for loop, for list, set and map.

2.Entity:
    - public class Dog

3.Method Signature :
    - public void sound()
    - public static <T> boolean checkInterface(Class<?> theClass)
    
4.Jobs to be Done:
    1.Create a class Dog and implementing Animal interface with single sound method().
    2.Declare a method public static <T> boolean checkInterface(Class<?> theClass) returning theClass.isInterface()
    3.In the sound() method printing "Barking" and declare main method.
    4.Integer class , Dog class and Animal interface 
    4.Declare integer class and printing boolean , getClass() method for getting class and getName() method for getting type
    5.Using TryCatch exception handling Checking class , getClass and getName present or not
    
 Pseudo Code:
 	interface Animal {
		public void sound();
	}
	public class Dog implements Animal {
		 public static void main(String[] args) {
		 	boolean boolean1 = checkInterface(intClass);
		 	// print the getclass() and getName()
		 	boolean boolean2 = checkInterface(Dog.class);
		 	// print the getclass() and getName()
		 	boolean boolean3 = checkInterface(Animal.class);
		 	// print the getclass() and getName()		 	
		 }
	}


*/

package com.kpriet.java.generic;


interface Animal {
	public void sound();
}

public class Dog implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         

        boolean boolean2 = checkInterface(Dog.class);
        System.out.println(boolean2);                   
        System.out.println(Dog.class.getClass());       
        System.out.println(Dog.class.getName());        

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   
        System.out.println(Animal.class.getClass());    
        System.out.println(Animal.class.getName());     

        try {
            Class<?> errClass = Class.forName("Dog");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}