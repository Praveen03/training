/*Generics-Implementing Iterable interface:

1. Write a program to print employees name list by implementing iterable interface.

WBS :

 1.Requirements:
  		To write a program to print employees name list by implementing iterable interface.
  		
 2.Entity:
  	    MyIterable
  	    IterableInterface
  
 3.Method Signature:
  	    public MyIterable(T[] t),
  	   	public Iterator<T> iterator().
  
 4.Jobs To Be Done:
  		1.Creating the MyIterable class that implements the Iterable.
  		2.Creating the local variable list as generic type.
  		3.Creating a method MyIterable of generic type  and assigning already defined String values from the main class.
  	    4.Creating the main class as IterableInterface .
  		5.Declaring the String values in the name variable.
  		6.Creating the object myList for the variables to be iterated.
  		7.After assigning the variables in the list by iterator implement, the final result is printed.
 
 Pseudo Code :
	class MyIterable<T> implements Iterable<T> {
		private List<T> list;
		public MyIterable(T[] t) {
			list = Arrays.asList(t);
		}
		public Iterator<T> iterator() {
			return list.iterator();
		}
	}
	public class IterableInterface {
	
		public static void main(String[] args) {
			String[] nameList = {"Arjun", "RamKumar", "Kishore", "Raju"};
			// call the MyIterable<> method interface
			// print all the elements in the result
		}
	}

*/

package com.kpriet.java.generic;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		String[] nameList = {"Arjun", "RamKumar", "Kishore", "Raju"};
		MyIterable<String> names = new MyIterable<>(nameList);
		for (String string : names) {
			System.out.println(string);
		}
	}
}