/*
Question :
	1. Sort the roster list based on the person's age in descending order using java.util.Stream
WBS

1.Requirement:
	Sort the roster list based on the person's age in descending order using java.util.Stream

2.Entity:
	SortStream

3.Jobs to be done:
	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. sort the list based on the person age in descending order
	4. Iterating the list to print person details

Pseudo Code:

public class SortOrderUsingComparator {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.

        list.sort(Comparator.comparing(Person::getAge).reversed());
        // sort method requires Comparator as parameter
        // Comparator has a method comparing which accepts function as parameter
        // reversed() method reverses the sorted list.
}
*/

package com.kpriet.java.streams;

import java.util.List;

public class SortStream {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
        // stream() method can pipeline many different methods to get desired
        // results in single line of code
        // sorted() method accepts comparator here compareByAge method of person
        // class is passed
        // finally forEach to print person details
    }
}
