/*
Question:
	9) Sort the roster list based on the person's age in descending order using comparator

WBS

1.Requirement:
	Sort the roster list based on the person's age in descending order using comparator

2.Entity:
	SortDescending

3.Jobs to be done:
	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. sort the list based on the person age in descending order
	4. Iterating the list to print person details

Pseudo Code:

public class SortOrderUsingComparator {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.

        list.sort(Comparator.comparing(Person::getAge).reversed());
        // sort method requires Comparator as parameter
        // Comparator has a method comparing which accepts function as parameter
        // reversed() method reverses the sorted list.
}
*/

package com.kpriet.java.streams;

import java.util.Comparator;
import java.util.List;

public class SortDescending {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());

        for (Person person : personList) {
            person.printPerson();
        }
    }
}

