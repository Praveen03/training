/*
Question :
	3. Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

WBS

1.Requirement:

Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

2.Entity:
	StreamFilter

3.Jobs to be done:	
	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. filter the list using stream API's filter method
	4. get first person from the filtered persons
	5. get last person from the filtered persons
	6. get random person from the filtered persons

Pseudo Code:

	public class FilterByGenderAndAge {
	    public static void main(String[] args) {
	        List<Person> list = person.createRoster();
	        // creating a list and storing the returned list from method.
	
	        List<Person> maleList = list.stream()
	            .filter(person -> person.gender == Person.Sex.MALE)
	            .collect(Collectors.toList());
	        // filtering based on the person's gender with Stream API
	
	        maleList.get(0);
	        // getting first person from list
	
	        maleList.get(list.size() - 1);
	        // getting last person from list
	
	        maleList.get(random.nextInt());
	        // using random object to create random number the range is given as parameter
		}
	}

*/

package com.kpriet.java.streams;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamFilter {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        //creating a roster  of persons

        List<Person> maleRosterList = rosterList.stream()
                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                .collect(Collectors.toList());
        //using Stream API filter getting the persons who are male.

        maleRosterList.get(0).printPerson();
        //getting the first person in the male list

        maleRosterList.get(maleRosterList.size() - 1).printPerson();
        //getting the last person in the male list

        Random random = new Random();
        maleRosterList.get(random.nextInt(maleRosterList.size())).printPerson();
        //using the object of Random class getting a random number given the range
        //as list size
        //the printPerson() method prints the person name along with his age.
    }

}
