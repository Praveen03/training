/*
Question :
	1) Write a program to filter the Person, who are male and age greater than 21

WBS 

1.Requirement:
	Write a program to filter the Person, who are male and age greater than 21

2.Entity:
	FilterGenderAge

3.Jobs to be done:

	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list
	3. Iterating the rosterList using for loop
	4. Condition to check the person male and his age greater than 21
	    4.1. If satisfied print the person name and age.

Pseudo Code:

public class FilterGenderAge {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.
        
        if (gender == MALE && age > 21) {
            (Filtering the list using age and gender)
        }
    }
}
*/

package com.kpriet.java.streams;

import java.util.List;

public class FilterGenderAge {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        //storing the returned list into rosterList
        for (Person person : rosterList) {
            if ((person.getGender() == Person.Sex.MALE) && (person.getAge() > 21)) {
                person.printPerson();
            }
        }
        // iterating all persons in list and accessing age and gender to check
        // the condition matches

    }

}
