/*
Question
	7. Consider a following code snippet:
         List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
         Get the non-duplicate values from the above list using java.util.Stream API
WBS

1.Requirement:
	Consider a following code snippet:
       List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
       Get the non-duplicate values from the above list using java.util.Stream API

2.Entity:
	StreamDuplicate

3.Jobs to be done:
	1. Create class StreamDuplicate and main method.
	2. Create list for randomNumber and give the value for array.
	3. Using collect() method find the non-duplicate values from the above list.
	4. Display the result using steram API.
	
	
Pseudo Code :

	public class StreamDuplicate {
		
		public static void main(String args[]) {
			
			List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
			System.out.println("List with duplicates: " + randomNumbers); 
			
			List<Integer> withoutDupes = randomNumbers.stream().distinct() 
					.collect(Collectors.toList()); 
			System.out.println("List without duplicates: " + withoutDupes);
		}
	}
  
*/
package com.kpriet.java.streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDuplicate {
	
	public static void main(String args[]) {
		
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
		System.out.println("List with duplicates: " + randomNumbers); 
		
		List<Integer> withoutDupes = randomNumbers.stream().distinct() 
				.collect(Collectors.toList()); 
		System.out.println("List without duplicates: " + withoutDupes);

	}
}
