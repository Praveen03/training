/*
Question :
	2. Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API

WBS

1.Requirement:
	Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API

2.Entity:
	PersonNameMail

3.Jobs to be done:
	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. Iterate the list and get the name and mail
	4. put email as key and name as value to map
	5. Displaying the map elements using forEash method in stream

Pseudo Code:

public class FilterByGenderAndAge {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.

        Map<String, String> map = new HashMap<>();
        for (Person roster : list) {
            map.put(roster.email, roster.name);
        }
        // mapping elements

        map.forEach(System.out::println);
        // printing the elements of map using Stream API
    }
}
*/

package com.kpriet.java.streams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonNameMail {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();

        Map<String, String> mapDetails = new HashMap<>();

        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }

        //Another way to do the same
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        // Using stream() method the collections of objects parallely processed
        // The collect method act to each and every object.
        // the toMap method of Collectors class maps the two functions which 
        // return string values.

        rosterMap.entrySet().forEach(System.out::println);
    }

}
