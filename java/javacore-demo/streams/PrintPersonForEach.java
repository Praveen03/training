/*
Question :
	8. Print all the persons in the roster using java.util.Stream<T>#forEach 

WBS

1.Requirement:
	Print all the persons in the roster using java.util.Stream<T>#forEach

2.Entity:
	PrintPersonForEach
	
3.Jobs to be done:

	1. Call the createRoster static method using the class, the method returns a list
	2. Store the returned list to a new list
	3. Stream API's forEach used to access the printPerson method of all objects.

Pseudo Code:

public class PrintPersonForEach {
    public static void main(String[] args) {
        List<Person> list = person.createRoster();
        // creating a list and storing the returned list from method.
        list.forEach(Person::printPerson);
        //stream API's forEach access objects of list sequentially and the 
        //method referenced to each object of the class.
     }
}

*/

package com.kpriet.java.streams;

import java.util.List;

public class PrintPersonForEach {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);
    }
}
