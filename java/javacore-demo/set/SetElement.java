/*
Question :
	Java program to demonstrate adding elements, displaying, removing, and iterating in hash set

WBS :

1.Requirements
    Java program to demonstrate adding elements, displaying, removing, and iterating in hash set.
    
2.Entities
    SetElement
   
3.Jobs to be done
   1.Create a SetElement and declaring main method.
   2.Using in-build function add() method to insert values.
   3.Using in-build function perform remove() method.
   4.Display the set using while loop with hasNext() and forEach.
   5.Using iterator class iterating the hash set.
   
 Pseudo Code :
 
	 public class SetElement {
	 	public static void main(String[] args) {
			Set<String> apps = new HashSet<>();
			using add() method all elements
			apps.remove("Chrome");
			for (String display : apps)
	          System.out.print(display +" ");
	        Iterator<String> app = apps.iterator();
	        while (app.hasNext()) {
	            System.out.print(app.next()+" ");
	        }
		}
	 }
      
*/
package com.kpriet.java.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetElement {
	public static void main(String[] args) {
		
		//Creating a set and adding elements to it.
		Set<String> apps = new HashSet<>();
        apps.add("Youtube");
        apps.add("Amazon");
        apps.add("Flipkart");
        apps.add("Instagram");
        apps.add("Facebook");
        apps.add("MX Player");
        apps.add("Chrome");
        apps.add("Drive");
        apps.add("Gmail");
        apps.add("Maps");
        System.out.println("SET : " + apps +"\n");
        
         //Perform remove() to the set.
        System.out.print("remove() method : ");
        apps.remove("Chrome");
        System.out.print(apps+"\n");
        System.out.println();

        //Displaying all the set elements using ForEach
        System.out.println("Display all Elements :");
        for (String display : apps)
          System.out.print(display +" ");

        //Displaying all the set elements using Iterator interface
        Iterator<String> app = apps.iterator();
        System.out.println("\n\nIterating in hash set :");
        while (app.hasNext()) {
            System.out.print(app.next()+" ");
        }
      
	}
}
