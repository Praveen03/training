/*
Question :
	2.write a program for email-validation?
	
WBS :

1.Requirement :
	Write a program for email-validation.

2.Entities :
	EmailValidation
	
3.Job to be Done :
	1.Create class EmailValidation and main method.
	2.Create string mail.
	3.Using pattern class and enter the condition for e-mail validation in compile method.
	4.Using matcher class, check wheather pattern class matches the mail.
	5.If matcher find print it is valid mail id and else not valid.
	
Pseudo Code :
	public class EmailValidation {
		 
	    public static void main(String[] args) {
	        
	        String mail = "abcdef@gmail.com";
	        Pattern p = Pattern.compile("[a-z]{6}[@]");
	        Matcher m = p.matcher(mail);   
	        if(m.find()){
	            System.out.println(mail + " is valid mail id");
	        }
	        else {
	            System.out.println(mail + " is not valid mail id");
	        }
	    }
	}

	
*/

package com.kpriet.java.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
	 
    public static void main(String[] args) {
        
        String mail = "abcdef@gmail.com";
        Pattern p = Pattern.compile("[a-z]{6}[@]");
        Matcher m = p.matcher(mail);   
        if(m.find()){
            System.out.println(mail + " is valid mail id");
        }
        else {
            System.out.println(mail + " is not valid mail id");
        }
    }
}
