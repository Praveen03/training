/*
Regex :
  2.Split any random text using any pattern you desire.

1.Requirements:
     Split any random text using any pattern you desire.
   
2.Entities
     SplitText
   
4.Jobs to be done
  1.Create a Class name SplitText and declare main method.
  2.Create a string and initialise a text to split.
  3.Create a array to split text to array using split() method with particular value.
  4.Print text using for each loop.
 
*/

package com.kpriet.java.regex;

public class SplitText{  
	public static void main(String args[]){  
		String text = "java regex string split method";
		String[] words = text.split("\\s");//splits the string based on whitespace   
		for(String display : words){  
		System.out.println(display);  
		}  
    }
}