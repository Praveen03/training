/*
Question :

2.)write a program to display the following using escape sequence in java
   A.My favorite book is "Twilight" by Stephanie Meyer
   B.She walks in beauty, like the night, 
	  Of cloudless climes and starry skies 
	  And all that's best of dark and bright 
	  Meet in her aspect and her eyes�
   C."Escaping characters", � 2019 Java
	  
WBS :

1.Requirement :
	write a program to display the following using escape sequence in java
	   A.My favorite book is "Twilight" by Stephanie Meyer
	   B.She walks in beauty, like the night, 
		  Of cloudless climes and starry skies 
		  And all that's best of dark and bright 
		  Meet in her aspect and her eyes�
	   C."Escaping characters", � 2019 Java
	  
2.Entities :
	EscapeSequence
	
3.Job to be Done :
	1.Create class EscapeSequence and main method.
	2.Create string for the give A,B and C.
	3.Using the escape sequence print the following A,B and C. 

Pseudo Code :
public class EscapeSequence {
	
	public static void main(String[] args) {
		
		// use the escape sequence for the given string 
		// print the result
	    
	}
}

*/

package com.kpriet.java.regex;

public class EscapeSequence {
	
	public static void main(String[] args) {
		
		String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println("A: "+myFavoriteBook);
	    
	    String sentence = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");    
	    System.out.println("\nB: "+ sentence);
	    
	    System.out.println("\nc: \"Escaping characters\", \u00A9 2019 CodeGym");
	    
	}
}
