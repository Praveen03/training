/*  Which class would you use to store your birthday in years, months, days, seconds, and nanoseconds?
 
 Ans:
 
   LocalDateTime class:
           To take a particular time zone into account, you would use the ZonedDateTime class. 
          Both classes track date and time to nanosecond precision and both classes, when used in years, months, and days.     
   
 */
