/*
 Question :
 	5. Write an example that, for a given year, reports the length of each month within that particular year.
 WBS :
 
 Requirement :
    Write an example that, for a given year, reports the length of each month within that particular year. 	

 Entities :
 	LengthOfMonth
 	
 Job do be Done :
 	1. Create class LengthOfMonth and main method.
 	2. Give the year using Year.of() method.
 	3. Using the scanner class get the month form user.
 	   3.1). Enter the month in inteeger.
 	4. Use the month class get month.
 	5. Use method length() get no of days in year.
 	6. Use method maxLength() get no of days in month
 	
 Pseudo Code :
 public class LengthOfMonth {
    public static void main(String[] args) {
        Year year = Year.of(2001);
        // create scanner class for get month
        // create month class
        // print no of days in year using length() method
        // print  no of days in month using maxLength() method
     }
 }
*/

package com.kpriet.java.datetime;

import java.time.Month;
import java.time.Year;
import java.util.Scanner;

public class LengthOfMonth {
    public static void main(String[] args) {
        Year year = Year.of(2001);
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter Month in 1-12 :");
		int monthdate = scanner.nextInt();
        Month month = Month.of(monthdate);
        System.out.println("Year : " + year.length());
        System.out.println("Days : " + month.maxLength());
        scanner.close();
    }
}