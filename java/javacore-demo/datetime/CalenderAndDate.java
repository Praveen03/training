/*
Problem Statement
	1. How do you convert a Calendar to Date and vice-versa with example? 

WBS :

1.Requirement :
      Convert a Calendar to Date 

2.Entity:
      CalculateAge

3.Jobs to be done :
    1.Invoke the LocalDate class and pass my date of birth in of LocalDate class method.
    2.Invoke the LocalDate class and get current time using now method.
    3.Inoke Period class pass dateOfBirth and now parameters in between method
    4.Get the my age using Period object with getYears, getMonths and getDays method
      
Pseudo Code:
 
public class CalculateAge {  
   public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2000, 9, 16);
        LocalDate now = LocalDate.now();
        Period difference = Period.between(dateOfBirth, now);
     System.out.printf("My age %d , %d months and %d days old.\n\n", 
                    difference.getYears(), difference.getMonths(), difference.getDays());
   }
}

*/
package com.kpriet.java.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalenderAndDate {
	// Convert Date to Calendar
	private Calendar dateToCalendar(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;

	}

	// Convert Calendar to Date
	private Date calendarToDate(Calendar calendar) {
		return calendar.getTime();
	}

	public static void main(String[] argv) throws ParseException {

		// 1. Create a Date from String
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "17-03-2020 10:20:56";
		Date date = sdf.parse(dateInString);
		CalenderAndDate convertor = new CalenderAndDate();

		// 2. Test - Convert Date to Calendar
		Calendar calendar = convertor.dateToCalendar(date);
		System.out.println(calendar.getTime());

		// 3. Test - Convert Calendar to Date
		Date newDate = convertor.calendarToDate(calendar);
		System.out.println(newDate);

	}
}
