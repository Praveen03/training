/*
 Question :
 	 4. Write a Java program to calculate your age.
 	 
 WBS :
 
 Requirement :
      Write a Java program to calculate your age 

 Entities :
 	CalculateAge
 	
 Job do be Done :
 	1.Create LocalDate class and get year, month, date.
 	2.Using LocalDate class get the current date now().
 	3.Using Period class find the difference between thes two date.
 	4.Display the result.
 	 	
 Pseudo Code :
	 public class CalculateAge {
	    public static void main(String[] args) {
	        LocalDate local = LocalDate.of(1998, 04, 23); 
			LocalDate now = LocalDate.now(); 
		    Period diff = Period.between(local, now); 
			System.out.println("Age : "+diff.getYears());
	     }
	 }
*/

package com.kpriet.java.datetime;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

public class CalculateAge {
	public static void main(String[] args) {
		LocalDate local = LocalDate.of(1998, 04, 23); //specify year, month, date directly
		LocalDate now = LocalDate.now(); //gets localDate
	    Period diff = Period.between(local, now); //difference between the dates is calculated
		System.out.println("Age : "+diff.getYears());
	}
} 

