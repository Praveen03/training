/*
 Question :
 	4. Write an example that tests whether a given date occurs on Tuesday the 11th.

 WBS :
 
 Requirement :
 	 Write an example that tests whether a given date occurs on Tuesday the 11th.

 Entities :
  	TuesdayEleventh
  	
 Method Signature:
 	class isTuesday implements TemporalQuery<Boolean>
  
 Job to be Done :
 	1. Create class TuesdayEleventh and main method.
 	2. Create scanner class for getting year, month and date as (YYYY-MM-DD).
 	3. Create class LocalDate for getting date.
 	4. Call the isTuesday() method .
 	   4.1 check the day of month is 11 and day of week is 2.
 	   4.2 if the condition is true returns true otherwise false.
 	5. Display the result.
 	
 Pseudo Code :
 class isTuesday implements TemporalQuery<Boolean> {
    // check the day of month is 11 and day of week is 2.
 	 return;
 }
 public class TuesdayEleventh {

	public static void main(String[] args) {
		 Scanner scanner = new Scanner(System.in);
		 // getting year, month and date as (YYYY-MM-DD)
		 LocalDate thisDate = LocalDate.parse(date);
		 // call the isTuesday() method.
		 
	 }
 }
 

*/

package com.kpriet.java.datetime;

import java.time.temporal.TemporalQuery;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.ChronoField;

import java.util.Scanner;
import java.time.LocalDate;

import java.lang.Boolean;

class isTuesday implements TemporalQuery<Boolean> {

	// Returns TRUE if the date occurs on Tuesday the 11th.
	public Boolean queryFrom(TemporalAccessor date) {

		return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
	}
}

public class TuesdayEleventh {

	public static void main(String[] args) {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.println("Enter the year,month and date as(YYYY-MM-DD)");
			String date = scanner.next();
			LocalDate thisDate = LocalDate.parse(date);
			System.out.println(thisDate.query(new isTuesday()));
			System.out.println("Press 1 to continue else 0 to exit");
			choice = scanner.nextInt();
		}while(choice == 1);
		System.out.println("Program executed successfully");
		scanner.close();
	}
}