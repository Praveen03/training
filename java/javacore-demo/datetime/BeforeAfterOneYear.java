/*
Question :
	10.Write a Java program to get a date before and after 1 year compares to the current date. 

WBS :

1.Requirement :
      Program to get a date before and after 1 year compares to the current date.

2.Entity:
      BeforeAfterOneYear

3.Jobs to be done :
     1.Invoke Calendar class getInstance method and store in calendar.
     2.Invoke Calendar class getTime method and store date in date variable.
     3.Using add method add one year to Calender
          3.1)Get the add one year using Calender class getTime method and store it in nextYear.
     4.Using add method subract one year to Calender
          3.1)Get the subract one year using Calender class getTime method and store it in  previousYear.
     5.Print the current date, nextYear and previousYear.
     
Pseudo Code:
	
	public class BeforeAfterOneYear {
	   public static void main(String[] args) {
	      Calendar calendar = Calendar.getInstance();
	      Date date = calendar.getTime();
	      calendar.add(Calendar.YEAR, 1); 
	      Date nextYear = calendar.getTime();
	      calendar.add(Calendar.YEAR, -2); 
	      Date previousYear = calendar.getTime();
	      System.out.println("\nCurrent Date : " + date);
	      System.out.println("\nDate before 1 year : " + previousYear);
	      System.out.println("\nDate after 1 year  : " + nextYear+"\n");  	
	    }
	}
*/

package com.kpriet.java.datetime;

import java.util.Calendar;
import java.util.Date;
public class BeforeAfterOneYear {
   public static void main(String[] args) {
      Calendar calendar = Calendar.getInstance();
      Date date = calendar.getTime();
      // get next year
      calendar.add(Calendar.YEAR, 1); 
      Date nextYear = calendar.getTime();
      //get previous year
      calendar.add(Calendar.YEAR, -2); 
      Date previousYear = calendar.getTime();
      System.out.println("\nCurrent Date : " + date);
      System.out.println("\nDate before 1 year : " + previousYear);
      System.out.println("\nDate after 1 year  : " + nextYear+"\n");  	
    }
}
