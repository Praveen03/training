/*  
 Question:
 	Write a program to print the volume of a Rectangle using lambda expression.

1.Requirements
     Program to print the volume of a Rectangle using lambda expression.
     
2.Entities
     VolumeRectangle
     Volume (Interface)
     
3.Method Signature
     int Values(int base,int length,int height);
     
4.Jobs to be done
   1.Create a class as VolumeRectangle with interface as PrismValues.
   2.Inside the declaring the single method with integer parameters.
   3.In the class main creating interface object and assigning with passing prism values returning prism value.
   4.Print statement invoking the interface single method with multiple integer value and finally return value using assigned
     interface object lambda expression.
     
Pseudo Code:
	interface Volume {
		int Values(int base,int length,int height);
	}
	public class VolumeRectangle {
		public static void main(String[] args) {
			Volume volume = (base,length,height) -> base * length * height;
			// pass the value to volume interface
		}
	}



*/
package com.kpriet.java.lambda;

interface Volume {
	int Values(int base,int length,int height);
}


public class VolumeRectangle {

	public static void main(String[] args) {
		Volume volume = (base,length,height) -> base * length * height;
		System.out.println(volume.Values(5,8,16));

	}
}
