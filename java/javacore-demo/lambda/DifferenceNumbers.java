/*
 Question :
 	Write a program to print difference of two numbers using lambda expression and the single method interface

1.Requirements :
    Program to print difference of two numbers using lambda expression and the single method interface
    
2.Entities :
    DifferenceNumbers
    Different (Interface)
    
3.Method Signature :
     int numbers(int number1,int number2);
     
4.Jobs to be done :
   1.Create a class as Different with interface as DifferenceNumbers
   2.Inside the declaring the single method with two integer parameters.
   3.In the class main creating interface object and assigning the lambda expression to return two integers difference.
   4.Print statement invoking the interface single method with integer values and finally return the value using assigned
     interface object lambda expression 
     
 Pseudo Code:
	interface Different {
		int numbers(int number1,int number2);
	}
	
	public class DifferenceNumbers {
		public static void main(String[] args) {
			Different different = (number1,number2) -> number1 - number2;
			// create the scanner class getting 2 value from user
			// pass the two value to Different interface and print the result
		}
	}

*/

package com.kpriet.java.lambda;

import java.util.Scanner;

interface Different {
	int numbers(int number1,int number2);
}

public class DifferenceNumbers {
	public static void main(String[] args) {
		Different different = (number1,number2) -> number1 - number2;
		try (Scanner scanner = new Scanner(System.in);) {
			int a = scanner.nextInt();
			int b = scanner.nextInt();
			System.out.println(different.numbers(a,b));
		}		
	}
}
