/*
 Question :
 	 Write a java program of performing two tasks by two threads that implements Runnable Interface.
 	 
 Requirement:
   Write a java program of performing two tasks by two threads that implements Runnable Interface.
   
 Entity:
   Multitasking
   
 Jobs to be done:
	1.Create two classes Task1 and Task2 which implements runnable interface.
	2.Create a method run() on both classes.
	3.In main class create a threads t1 and t2 and pass the objects of both class.  
	4.Start the thread process.
	
 Pseudo code:
 // creating two classs Task1 and Task2 implementing runnnable interface.
 // creating run() method.
 public class Multitasking {
	 public static void main(String[] args) {
		 Thread t1=new Thread(new Task1());
		 Thread t2=new Thread(new Task2());
         // call the run() method using tread t1 and t2
	 }
 }
 	
*/

package com.kpriet.java.multitasking;

class Task1 implements Runnable {
	 public void run(){
		 System.out.println("Start task one");
	 }
}
class Task2 implements Runnable {
	 public void run(){
		 System.out.println("Start task two");
	 }
}
public class Multitasking {
	 public static void main(String[] args) {
		 Thread t1=new Thread(new Task1());
		 Thread t2=new Thread(new Task2());
         t1.start();
         t2.start();
	 }
}