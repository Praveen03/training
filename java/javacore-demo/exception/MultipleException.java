/*
Requirements:
    Demonstrate the catching multiple exception with example.
     
Entity:
    MultipleExceptionExample
      
Jobs to be done:
  1. Declare and Initialize the array value in try block.
  2. Execute the try block.
  3. When an exception occurs in try block ,then go to the catch block.
  4. Check the exception, which are known to compile time.
  5. If two or more exception in this catch block ,then it's separated by pipe character |.
  6. Display the message. 
  
Pseudo Code :
	public class MultipleException {    
   		public static void main(String[] args) {    
	       try {    
	           int array[] = new int[10];    
	           array[10] = 30/0;    
	           }
	       catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
	           System.out.println(e.getMessage());
	           }
	       catch(Exception e) { 
	           System.out.println(e.getMessage());  
	       }
   		}    
	}

*/
package com.kpriet.java.exception;

public class MultipleException {    
   public static void main(String[] args) {    
       try {    
           int array[] = new int[10];    
           array[10] = 30/0;    
           }
       catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
           System.out.println(e.getMessage());
           }
       catch(Exception e) { 
           System.out.println(e.getMessage());  
       }
   }    
}
