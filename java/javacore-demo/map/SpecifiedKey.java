/*
 1)Write a Java program to copy all of the mappings from the specified map to another map?

1.Requirements
     Java program to copy all of the mappings from the specified map to another map.
     
2.Entities
     SpecifiedKey

3.Jobs to be done
   1.Create a class as SpecifiedKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called bikes.
   3.Adding the values using put() method and creating another hashMap for copying using putAll() method.
   4.Printing the copied hashMap bikes2.
   
Pseudo Code

	public class SpecifiedKey {
	
	   public static void main(String args[]) {
		   
	      HashMap<Integer, String> car = new HashMap<Integer, String>();
	
	       // using put() method to add values to map
	      
	      HashMap<Integer, String> cars = new HashMap<Integer, String>();
	            
	      //Copy all of the mappings from the specified map to another map
	      cars.putAll(car);
	      System.out.println(cars);
	   }
	}

*/
package com.kpriet.java.map;

import java.util.HashMap;

public class SpecifiedKey {

   public static void main(String args[]) {
	   
      HashMap<Integer, String> car = new HashMap<Integer, String>();

      car.put(32, "BMW");
      car.put(15, "Tesla");
      car.put(5, "Honda");
      car.put(16, "Mahandra");
      car.put(25, "Skoda");
      
      HashMap<Integer, String> cars = new HashMap<Integer, String>();
            
      //Copy all of the mappings from the specified map to another map
      cars.putAll(car);
      System.out.println(cars);
   }
}