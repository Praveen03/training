/*
 Question :
 	2) Write a Java program to test if a map contains a mapping for the specified key?

1.Requirements
     Java program to copy all of the mappings from the specified map to another map.
     Count the size of mappings in a map
     
2.Entities
     ContainsKey
    
3.Jobs to be done
   1.Create a class as ContainsKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called car.
   3.Adding the values using put() method and using containsKey() method find the specified key value map is containing or not.
   4.Count the size of mappings in a map using size() method.
  
Pseudo Code:
	public class ContainsKey {
		 public static void main(String args[]) { 
			  //Creating hashMap 
		      HashMap<Integer, String> car = new HashMap<Integer, String>();
		      // add keys and value using put() method
		      // using constainsKey() method check if key is present print true otherwise false
		      // using size() method to get size of map 
		  }
	}


*/
package com.kpriet.java.map;

import java.util.HashMap;

public class ContainsKey {
	 public static void main(String args[]) {
		   
		  //Creating hashMap 
	      HashMap<Integer, String> car = new HashMap<Integer, String>();

	      car.put(34, "BMW");
	      car.put(2, "Tesla");
	      car.put(13, "Honda");
	      car.put(23, "Mahandra");
	      car.put(11, "Skoda");
	      
	      System.out.println("Key value map containing or not : " + car.containsKey(2));
	      
	      System.out.println("Key value map containing or not : " + car.containsKey(10));
	      
	      System.out.println("Count the size of mappings in a map : " + car.size());
	      
	   }
}
