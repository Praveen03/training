package com.kpr.training.jetty.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kpr.training.jetty.exception.AppException;


public class AuthenticationFilter implements Filter{
	
	private FilterConfig filter = null;

	@Override
	public void destroy() {
		filter = null;
	}

	@Override
	public void doFilter(ServletRequest req1, ServletResponse res1, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest req = (HttpServletRequest) req1;
			HttpSession session = req.getSession(false);
			if(session == null) {
				chain.doFilter(req1, res1);
			}
		} catch(Exception e) {
			throw new AppException(ErrorCode.E439);
		}
	}

	@Override
	public void init(FilterConfig filter) throws ServletException {
		this.filter = filter;
	}

}
