package com.kpr.training.jetty.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.kpr.training.jetty.exception.ErrorCode;
import com.kpr.training.jettyconnections.ConnectionService;


public class TransactionFilter implements Filter {
	
	private FilterConfig filter = null;

	@Override
	public void destroy() {
		filter = null;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		try {
			ConnectionService.get();
			chain.doFilter(req, res);
		} catch (Exception e) {
			throw new AppException(ErrorCode.E438);
		}
	}

	@Override
	public void init(FilterConfig filter) throws ServletException {
		this.filter = filter;
	}

}
