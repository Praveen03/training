package com.kpr.training.jetty.constant;

public class QueryStatement {
	
	public static final String INSERT_ADDRESS_QUERY 	= new StringBuilder("INSERT INTO address ")
															  .append("(street             ")
                                                              .append(", city              ")
															  .append(", postal_code)      ")
															  .append("VALUES (?,?,?)      ")
															  .toString();
																			
	public static final String UPDATE_ADDRESS_QUERY		=  new StringBuilder("UPDATE address ")
															  .append("   SET street = (?)")
															  .append(",        city = (?)")
															  .append(", postal_code = (?)")
															  .append(" WHERE (id = (?))  ")
															  .toString();												
	
	
	public static final String READ_ADDRESS_QUERY 		=  new StringBuilder("SELECT street       ")
														      .append(", 	   city         ")
														      .append(",      postal_code  ")
														      .append("  FROM address      ")
														      .append(" WHERE id = (?)     ")
														      .toString();
																			
	public static final String READ_ALL_ADDRESS_QUERY	= new StringBuilder("SELECT id     	   ")
														     .append("      ,street 	   ")
															 .append("      ,city        ")
															 .append("      ,postal_code ")
															 .append("  FROM address")
															 .toString();
																	 
	public static final String DELETE_ADDRESS_QUERY 	=new StringBuilder("DELETE FROM address")
															 .append(" WHERE (id = (?))  ")
															 .toString();
					 
	
	public static final String INSERT_PERSON_QUERY 		= new StringBuilder(" INSERT INTO person")
														     .append("            (first_name ")
														     .append("   	    	,last_name")
														   	 .append("			,email")
														   	 .append("			,address_id")
														   	 .append("			,birth_date)")
														     .append(" VALUES (?,?,?,?,?)")
														     .toString();
																				  
	public static final String READ_PERSON_QUERY 		= new StringBuilder("SELECT 	  id          ")
															 .append("			, first_name  ")
															 .append("			, last_name   ")
															 .append("			, email       ")
															 .append("          , address_id  ")
															 .append("          , birth_date  ")
															 .append("          , created_date")
															 .append("  FROM person         ")
															 .append(" WHERE (id = (?))     ")
															 .toString();			
																			
	public static final String READ_ALL_PERSON_QUERY 	= new StringBuilder("SELECT  id	          ")
															 .append("      , first_name    ")
															 .append("      , last_name     ")
															 .append("	  , email         ")
															 .append("	  , address_id    ")
															 .append("	  , birth_date    ")
															 .append("	  , created_date  ")
															 .append("  FROM person         ")
															 .toString();
																			 
	public static final String DELETE_PERSON 			= new StringBuilder("DELETE FROM person")
															 .append(" WHERE (id = (?)) ")
															 .toString();
																		
	public static final String UPDATE_PERSON 			=  new StringBuilder("UPDATE person 		  ")
															  .append("   SET first_name = ? ")
															  .append(",      last_name = ?  ")
															  .append(",      email = ?	  ")
															  .append(",      birth_date = ? ")
															  .append(" WHERE id = ?		  ")
															  .toString();
																		
	public static final String UNIQUE_QUERY				=  new StringBuilder("SELECT id			 ")
															 .append("  FROM person		 ")
															 .append(" WHERE (email = (?)) ")
															 .append("   AND id != (?)     ")
															 .toString();
	
	public static final String UPDATE_UNIQUE_QUERY		= new StringBuilder("SELECT `id`      ")
															 .append("  FROM `jdbc`.`person` ")
															 .append(" WHERE `email` = (?)   ")
															 .append("  AND `id` != (?)     ")
														     .toString();

	
	public static final String ADDRESS_CHECK			= new StringBuilder("SELECT `id`      ")
														 	 .append("  FROM `jdbc`.`address`")
														 	 .append(" WHERE (`street` = (?))")
														 	 .append("   AND (`city` = (?))  ")
														 	 .toString();
	
	public static final String PERSON_CHECKER 			= new StringBuilder("SELECT id      ")
														  	 .append("  FROM person        ")
														  	 .append(" WHERE first_name = ?")
														  	 .append("   AND last_name = ? ")
														  	 .toString();
											
	public static final String SEARCH_QUERY				= new StringBuilder("SELECT `id`	  ") 
															 .append(",		`street`	  ") 
															 .append(",		`city`		  ")
															 .append(",		`postal_code`" )
															 .append(" FROM `jdbc`.`address` ")
															 .append("WHERE `street` = (?)   ")
															 .append(" AND `city` = (?) 	  ")
															 .append(" AND `postal_code`= (?)")
															 .toString();

	public static final String DELETE_NOT_USED_ADDRESS_QUERY = new StringBuilder("DELETE person, address ")
													            .append("FROM person ")
													            .append("LEFT JOIN address")
													            .append(" ON person.address_id = address.id ")
													            .append("WHERE person.id = ? ")
													            .toString();

}
