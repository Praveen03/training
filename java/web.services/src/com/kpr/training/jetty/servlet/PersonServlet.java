package com.kpr.training.jetty.servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kpr.training.jetty.model.Person;
import com.kpr.training.jetty.services.PersonService;


public class PersonServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	Person personClass = new Person();
	Person personGot;
	private static PersonService personService = new PersonService();
	private static JsonUtil jsonUtil = new JsonUtil();

	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		
		res.setContentType("text/html");
		res.setCharacterEncoding("UTF-8");
		
		try (PrintWriter writer = res.getWriter()) {
			
			BufferedReader reader = req.getReader();
			
			StringBuilder builder = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine())!=null) {
				builder.append(line);
			}
			
			Person newPerson = (Person) jsonUtil.deSerialize(builder.toString(), personClass);
			
			Person personGot = personService.read(newPerson.getId(), true);
			
			writer.append(personGot.getFirstName()+" "+personGot.getLastName());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			BufferedReader reader = req.getReader(); 
			PrintWriter writer = res.getWriter();
			StringBuilder personJson = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine())!= null) {
				personJson.append(line);
			}
			
			personGot = (Person) jsonUtil.deSerialize(personJson.toString(), personClass);
			
						
			long personId = personService.create(personGot);
			
			if(personId != 0) {
				writer.append("Person Creation Success : " + personId);
			} else {
				writer.append("Person Creation Failure");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void doPut(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			
			PrintWriter writer = res.getWriter();
			
			BufferedReader reader = req.getReader();
			
			StringBuilder personJson = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine())!= null) {
				personJson.append(line);
			}
			
			Person person = (Person) jsonUtil.deSerialize(personJson.toString(), personClass);
			
			personService.update(person);
			
			res.setContentType("text/html");
			res.setCharacterEncoding("UTF-8");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	protected void doDelete(HttpServletRequest req, HttpServletResponse res) {
		
		try {
			
			BufferedReader reader = req.getReader();
			
			StringBuilder personId = new StringBuilder();
			
			String line = null;
			
			while((line = reader.readLine()) != null) {
				personId.append(line);
			}
			
			Person person = (Person) jsonUtil.deSerialize(personId.toString(), personClass);
			
			personService.delete(person.getId());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
