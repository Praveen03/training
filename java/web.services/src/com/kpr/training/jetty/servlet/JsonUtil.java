package com.kpr.training.jetty.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {
	
	public Object deSerialize(String personJson, Object typeObj) {
		
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		
		return gson.fromJson( personJson, typeObj.getClass());
	}
	
	public String serialize(Object typeObj) {
		
		Gson gson = new Gson();
		
		return gson.toJson(typeObj.getClass());
	}

}
