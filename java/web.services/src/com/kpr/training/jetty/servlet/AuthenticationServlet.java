package com.kpr.training.jetty.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jetty.exception.AppException;
import com.kpr.training.jetty.services.AuthenticationService;

public class AuthenticationServlet extends HttpServlet{
	
	String loginToken = "qw67as12";

	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try(PrintWriter writer = res.getWriter()) {
			
			if(new AuthenticationService().userPassValidator(req.getParameter("userName"), 
					req.getParameter("password"))) {
				writer.append("Login Success ");
				writer.append("  ");
				writer.append("Token : " + loginToken);
			} else {
				throw new AppException(ErrorCode.E441);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.E441);
		}
	}


}
