/*
  Problem Statement
  1.Perform CRUD Operations for Address Service
  
  Entity
  1.Address
  2.AddressService
  3.AppException
  4.ErrorCode
  
  Method Signature
  1.public static long create(Address address)
  2.public List<Address> read(long id)
  3.public List<Address> readAll()
  4.public void update(Address address, long id)
  5.public static void delete(long id)
  6.public List<Address> search(Address address)
  7.private static void StatementSetter(PreparedStatement ps, Address address)
  8.public Address getValue(ResultSet result)
  
  Jobs to be Done
  1.Create a Address
  2.Read an Address
  3.Read all Addresses
  4.Update an Address
  5.Delete an Address
  6.Search Address
 
 */
package com.kpr.training.jetty.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jetty.constant.Constant;
import com.kpr.training.jetty.constant.QueryStatement;
import com.kpr.training.jetty.exception.AppException;
import com.kpr.training.jetty.exception.ErrorCode;
import com.kpr.training.jetty.model.Address;
import com.kpr.training.jetty.validator.AddressValidator;
import com.kpr.training.jettyconnections.ConnectionService;


public class AddressService {
	ResultSet result;
	Address address;
	Address readAddress;
	
	public static long create(Address address) throws Exception {
			
		AddressValidator.addressCodeValidator(address);
					
		long existAddress = AddressValidator.existAddress(address);
			
		if(existAddress == 0) {
			try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY)) {
				StatementSetter(ps, address);
				long create = ps.executeUpdate();
				return create;
			}catch(Exception e) {
				throw new AppException(ErrorCode.ADDRESS_CREATION_FAILD,e);	
				}
		}	
		else {
			throw new AppException(ErrorCode.ADDRESS_EXIST);
		}
	}
			
	 public List<Address> read(long id) {
			
		if (id == 0) {
			throw new AppException(ErrorCode.ADDRESS_ID_EMPTY);
		}
		else {
			try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {
				ps.setLong(1, id);
				List<Address> addressList = new ArrayList<>();	
				ResultSet result = ps.executeQuery();
				while(result.next()) {
					addressList.add(resultSetter(result));
				}
				System.out.print(addressList);
				return addressList;
			} catch(Exception e) {
				throw new AppException(ErrorCode.ADDRESS_READ_FAILD, e);
			}
		}
	}
	
	public List<Address> readAll() throws Exception {

		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)) {
			List<Address> addressList = new ArrayList<>();	
			ResultSet result = ps.executeQuery();
			while(result.next()) {
				addressList.add(resultSetter(result));
			}
			System.out.print(addressList);
			return addressList;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ADDRESS_READ_FAILD,e);
		}
	}
		
	public void update(Address address, long id) throws Exception {
		
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {	
			StatementSetter(ps, address);
			ps.setLong(4,id);
			ps.executeUpdate();
		} catch(Exception e) {
			throw new AppException(ErrorCode.ADDRESS_CREATION_FAILD,e);
		}	
		
	}
		
			
	public static void delete(long id) throws Exception {

		try (PreparedStatement statement = ConnectionService.getConnection().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {
			if(id != 0) {
				statement.setLong(1, id);
			    int result = statement.executeUpdate();
			    if (result == 0) {
			    	throw new AppException(ErrorCode.ADDRESS_ID_DELETE);
			    }
			}
			
		} catch(Exception e) {
			throw new AppException(ErrorCode.ADDRESS_DELETE_FAILD, e);
			}
	}
	
	
	public List<Address> search(Address address) {
		
		List<Address> addressList = new ArrayList<>();
		
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.SEARCH_QUERY)) {
			StatementSetter(ps, address);
			ResultSet resultSet = ps.executeQuery();
			if(resultSet != null) {
				while(resultSet.next()) {
					addressList.add(resultSetter(resultSet));
				}	
			}			
			return addressList;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ADDRESS_NOT_FOUND,e);
		}	
	}
	
	public Address resultSetter(ResultSet result) throws Exception {
			address = new Address(result.getLong(Constant.ID)
					,result.getString(Constant.STREET)
		   			,result.getString(Constant.CITY)
		   			,result.getLong(Constant.POSTAL_CODE));
		return address;
	}
	
	private static void StatementSetter(PreparedStatement ps, Address address) {
		try {
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getCity());
			ps.setLong(3, address.getPostalCode());
		} catch (Exception e) {
			throw new AppException(ErrorCode.PS_ERROR, e);
		}
	}
	
	public long deleteNotUsedAddress(Connection connection) throws Exception {
		long id = 0;
		try {
			PreparedStatement ps = connection.prepareStatement(QueryStatement.DELETE_NOT_USED_ADDRESS_QUERY);
			ResultSet result = ps.executeQuery();

			while (result.next()) {
				id = result.getLong(Constant.ID);
			}
		}catch (Exception e) {
			throw new AppException(ErrorCode.PERSON_DELETE_FAILD,e);
		}
		return id;
	}
	
	public static void main(String args[]) throws Exception {
		
		//Address ad = new Address("dscdasdscfd", "dsasf", 6565651);
		AddressService ads = new AddressService();
		//List<Address> addressList = ads.search(new Address("sdfs", "dsasf", 6565651));
		//System.out.print(addressList);
		//ads.create(ad);
		//ads.readAll();
		ads.read(10);
		//ads.update(ad, 1);
		//ads.delete(10);
		
	}
}
