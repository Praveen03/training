package com.kpr.training.jetty.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.kpr.training.jetty.constant.QueryStatement;
import com.kpr.training.jetty.exception.AppException;
import com.kpr.training.jettyconnections.ConnectionService;


public class AuthenticationService {
	
	public boolean userPassValidator(String userName, String password) throws Exception {
		try {
			Connection connection = ConnectionService.get();
			
			PreparedStatement ps = connection.prepareStatement(QueryStatement.GET_USER_ID_QUERY);
			
			ps.setString(1, userName);
			ps.setString(2, password);
			AuthenticationService.nullValidator(userName, password);
			
			ResultSet resultSet = ps.executeQuery();
			
			if(!(resultSet.next())) {
				return false;
			}
			
			return true;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodes.E437);
		}
	}
	
	
	private static void nullValidator(String userName, String password) {
		if(userName == ""  || 
		   userName == " "  || 
		   userName == null ||
		   password == ""   ||
		   password == " "  ||
		   password == null) {
			System.out.println("User Name and Password cannot be null or empty");
			throw new AppException(ErrorCode.E440);
		}
	}
	
}

