package com.kpr.training.jetty.validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kpr.training.jetty.constant.Constant;
import com.kpr.training.jetty.constant.QueryStatement;
import com.kpr.training.jetty.exception.AppException;
import com.kpr.training.jetty.exception.ErrorCode;
import com.kpr.training.jetty.model.Person;
import com.kpr.training.jettyconnections.ConnectionService;

public class PersonValidator {
	
	public static void personNullChecker(Person person) {
		if(person.getFirstName() == null || person.getLastName() == null || person.getEmail() == null || person.getBirthDate() == null) {
			throw new AppException(ErrorCode.CHECK_ALL_FIELDS);
		}	
	}
	
	
	public static long personNameCheck(Person person) {
		long name = 0;	
		try (PreparedStatement ps =  ConnectionService.getConnection().prepareStatement(QueryStatement.PERSON_CHECKER)) {	
			ps.setString(1, person.getFirstName());
			ps.setString(2, person.getLastName());
			ResultSet result = ps.executeQuery();
			while(result.next()) {
				name++;
			}
			return name;	
		} catch (Exception e) {
			throw new AppException(ErrorCode.PERSON_NAME_CHECK, e);
		}
		
	}	
	
	public static long emailUnique(Person person) throws Exception{	
		long email = 0;
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.UNIQUE_QUERY)) {
			ps.setString(1,person.getEmail());
			ResultSet result = ps.executeQuery();
			while(result.next() ) {
				email++;
			}
			return email;
		} catch(Exception e) {
			throw new AppException(ErrorCode.EMAIL_UNIQUE, e);
		}		
	}
	
	
	public long updateUnique(Person person) throws Exception {	
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.UPDATE_UNIQUE_QUERY)) {
			ps.setString(1, person.getEmail());
			ps.setLong(2, person.getId());
			
			ResultSet result = ps.executeQuery();
			
			while(!result.next()) {
				return 0;
			}
			return result.getLong(Constant.ID);
		} catch(Exception e) {
			throw new AppException(ErrorCode.PERSON_UNIQE);
		}

	}
	
	
	public static void personIdValidator(Person person) {
		if(person.getId() == 0) {
			throw new AppException(ErrorCode.PERSON_ID_EMPTY);
		}
	}
		
}
