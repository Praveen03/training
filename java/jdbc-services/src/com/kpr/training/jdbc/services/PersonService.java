package com.kpr.training.jdbc.services;

/*
  Problem Statement
  	Perform CRUD Operations for Person Service
  
  Entity
	  1.Person
	  2.PersonService
	  3.AppException
	  4.ErrorCode
  
  Method Signature
	  1.public long insert(Person person);
	  2.public Person readPerson(long id);
	  3.public List<Person> readAllPerson();
	  4.public long update(Person person);
	  5.public long delete(long id);
  
  Jobs to be Done
	  1.Create a Address
	  2.Read an Address
	  3.Read all Addresses
	  4.Update an Address
	  5.Delete an Address
 
*/

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jdbc.connections.ConnectionService;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validator.PersonValidator;


public class PersonService {
	
	Person person;

	public void create(Person person) throws Exception  {
		
		if(PersonValidator.personNameCheck(person) < 1 ) {
			if (PersonValidator.emailUnique(person) < 1) {
				try {
					
					 PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.INSERT_PERSON_QUERY);		
					 statementSetter(ps, person);
					 ps.setLong(5, person.getaddress());
					 ps.executeUpdate();  
					 
				} catch (Exception e) {
					throw new AppException(ErrorCode.PERSON_CREATION_FAILD, e);
				 	}
			}else {
				throw new AppException(ErrorCode.EMAIL_ID_EXIST);
			}
		}else {
			throw new AppException(ErrorCode.PERSON_EXSITS);
		}
			
	} 
	
	
	public List<Person> readPerson(long id)  {
				
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {
			ps.setLong(1,id);
			ResultSet result = ps.executeQuery();
			
			List<Person> personList = new ArrayList<>();
			while(result.next()) {
				personList.add(resultSetter(result));
			}
			System.out.print(personList);
			return personList;
			} catch(Exception e) {
				throw new AppException(ErrorCode.PERSON_READ_FAILD);
			}		
	}
	
	
	public List<Person> readAll() throws Exception {
		
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY)) {
			ResultSet result = ps.executeQuery();
			List<Person> personList = new ArrayList<>();
			while(result.next()) {
				personList.add(resultSetter(result));
			}
			System.out.print(personList);
			return personList;
		} catch (Exception e) {
			throw new AppException(ErrorCode.PERSON_READ_FAILD,e);
			}
	}
	
	public void update(Person person, long id) throws Exception {
	
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.UPDATE_PERSON)) {		
			statementSetter(ps, person);
			ps.setLong(5, id);
			ps.executeUpdate();
			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCode.PERSON_UPDATION_FAILD);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.PERSON_UPDATION_FAILD,e);
			}
	}
	
	
	public void delete(long id) throws Exception {		
					
		try {
			PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.DELETE_PERSON);
			ps.setLong(1, id);
			ps.executeUpdate();
			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCode.PERSON_DELETE_FAILD);
			}
		} catch (Exception e) {
			throw new AppException(ErrorCode.PERSON_DELETE_FAILD,e);
		}		
	}
	
	public Person resultSetter(ResultSet result) throws Exception {

		if (result.next()) {
			person = new Person(
						result.getLong(Constant.ID),
						result.getString(Constant.FIRST_NAME),
						result.getString(Constant.LAST_NAME),
						result.getString(Constant.EMAIL),
						result.getLong(Constant.ADDRESS),
						result.getDate(Constant.BIRTH_DATE),
						result.getTimestamp(Constant.CREATED_DATE));
			}
		return person; 			
	}

	private static void statementSetter(PreparedStatement ps, Person person) {
		try {
			ps.setString(1, person.getFirstName());
			ps.setString(2, person.getLastName());
			ps.setString(3, person.getEmail());
			ps.setDate(4,  person.getBirthDate());
			
		} catch (Exception e) {
			throw new AppException(ErrorCode.PS_ERROR, e);
		}
	}

	public static void main(String[] args) throws Exception {
		PersonService personService = new PersonService();
		//Date date = Date.valueOf("2001-01-26");
		    
	    //Person person = new Person("Ramrdtesd", "Rajzra", "rajarasdm@gmail.com", 1L, date);
	   
		personService.delete(5);
		//personService.readAll();
		//personService.readPerson(111);
		//personService.update(person, 5);
		//personService.create(person);
	}

}
