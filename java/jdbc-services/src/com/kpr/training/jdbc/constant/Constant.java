package com.kpr.training.jdbc.constant;

public class Constant {
	
	public static final String 
								URL                    = "URL",
								USERNAME               = "USERNAME",
								PASSWORD               = "PASSWORD",
							    ID                     = "id",
							    POSTAL_CODE            = "postal_code",
							    CITY                   = "city",
							    STREET                 = "street",
							    FIRST_NAME             = "first_name",
							    LAST_NAME              = "last_name",
							    EMAIL                  = "email",
							    ADDRESS				   =  "address_id",
							    BIRTH_DATE             =  "birth_date",
								CREATED_DATE 		   =  "created_date";
}