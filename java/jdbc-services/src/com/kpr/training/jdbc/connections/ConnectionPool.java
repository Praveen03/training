package com.kpr.training.jdbc.connections;

import com.mysql.jdbc.Connection;

public class ConnectionPool extends ThreadLocal<Connection>{
	
	public Connection initialValue() {
		return (Connection) ConnectionService.initConnection();
	}

	public void remove(Connection connection) {
		super.remove();
		try {
			ConnectionService.releaseConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void get(Connection connection) {
		try {
			ConnectionService.getConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
