/*
  Problem Statement
  	Create a AddressValidator to validate the address.
  	  
  Entity
	  AddressValidator
  
  Method Signature
	 1.public static void connection
     2.public static void addressCodeValidator
     3.public static void addressCodeValidator(Address address)
     4.public static void addressId(Address address) 
     5.public static void existAddress(Address address)
     
  Pseudo Code
	public class AddressValidator {

		Address address;
		
		public static void connection(Connection connection) {
			if(connection == null) {
				throw new AppException(ErrorCode.CONNECTION_FAILD);
			}
		}
		
		public static void addressCodeValidator(Address address) {
			if(address.getPostalCode() == 0) {
				throw new AppException(ErrorCode.POSTAL_CODE);
			}
		}
		
		public static void addressId(Address address) {
			if(address.getId() == 0) {
				throw new AppException(ErrorCode.ADDRESS_ID_EMPTY);
			}
		}
				
		public static void existAddress(Address address) throws Exception {
			
			if(address.getPostalCode() == 0) {
				throw new AppException(ErrorCode.POSTAL_CODE_EMPTY);
			}
			try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.ADDRESS_CHECK)) {
				ps.setString(1, address.getStreet());
				ps.setString(2, address.getCity());	
				ResultSet resultSet = ps.executeQuery();
				
				while(resultSet.next()) {
					resultSet.getLong("id");
				}
			} catch (Exception e) {
				throw new AppException(ErrorCode.ADDRESS_EXIST,e);
			}
			
			
		}	
	}

*/

package com.kpr.training.jdbc.validator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kpr.training.jdbc.connections.ConnectionService;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressValidator {

	Address address;
	
	public static void connection() {
		ConnectionService connection = (ConnectionService) ConnectionService.getConnection();
		if(connection == null) {
			throw new AppException(ErrorCode.CONNECTION_FAILD);
		}
	}
	
	public static void addressCodeValidator(Address address) {
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.POSTAL_CODE);
		}
	}
	
	public static void addressId(Address address) {
		if(address.getId() == 0) {
			throw new AppException(ErrorCode.ADDRESS_ID_EMPTY);
		}
	}
	
	public static long existAddress(Address address) throws Exception {
		
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.POSTAL_CODE_EMPTY);
		}
		try (PreparedStatement ps = ConnectionService.getConnection().prepareStatement(QueryStatement.ADDRESS_CHECK)) {
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getCity());	
			ResultSet resultSet = ps.executeQuery();
			
			while(!resultSet.next()) {
				return 0;
			}
			return resultSet.getLong(Constant.ID);
		} catch (Exception e) {
			throw new AppException(ErrorCode.ADDRESS_EXIST,e);
		}
		
		
	}

	public static void AddressNullChecker(Address address) {
		if (address.getPostalCode() == 0 || address.getStreet() == null || address.getCity() == null) {
			throw new AppException(ErrorCode.CHECK_ALL_FIELDS);
		}
	}	
}
