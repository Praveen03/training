/*
Problem Statement
	To Test All possible conditions for Address Service 

Requirement
	To Test All possible conditions for Address Service

Entity
	1.ServicesTest
	2.PersonService
	3.AppException
	4.Connection
	5.Person
	6.Address

Method Signature
	1.public void setUp()
	2.public void insertAddress()
	3.public void readAddress()
	4.public void readAllAddress()
	5.public void updateAddress()
	6.public void deleteAddress()
	7.public void deleteNotUsedAddress()
	8.public void connectionClose()

Jobs to be done:
	->public void setUp()
	  1.Prepare address object of type Address (pass values).
	  2.Prepare addressService object of type AddressService.
	
	@Test for insertAddress
	1.Invoke addressService insertAddress method (pass address object) 
	and store in insertAddressStatus.
	2.AssertJUnit assertTrue method (pass Check insertAddressStatus is greater than 0 ).
	
	@Test for readAddress
	1.Invoke addressService readAddress method (pass address id) and
	store in List<Address> objectList .
	2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
	
	@Test for readAddress
	1.Invoke addressService readAllAddress method and
	store in List<Address> objectList.
	2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
	
	@Test for updateAddress
	1.Invoke addressService updateAddress method (pass address object) 
	and store in udpateStatus.
	2.AssertJUnit assertTrue method (pass Check udpateStatus is not equal to false ).
	
	@Test for deleteAddress
	1.Invoke addressService deleteAddress method (pass address id) 
	and store in deleteStatus.
	2.AssertJUnit assertTrue method (pass Check deleteStatus is not equal to false ).

Psudeo Code:
	public class AddressServiceTest {
		long id;
		long addressUpdateId = 1;
		Address updatedAddress;
		Address addressRead;
		Address address1;
		Address address2;
		Address addressUpdate;
		AddressService addressService;
		Address deleteAddress;
		ConnectionService connectionService;
		Connection connection;
		
		@BeforeClass
		public void setUp() throws Exception {
			connectionService = new ConnectionService();
			connection = connectionService.initConnection();
			addressRead = new Address("Perumal", "Kanchipuram" , 601200);
			address1 = new Address("OMR", "Chennai" , 600028);
			address2 = new Address("Murugan", "Tiruchendhur", 641600);
			addressUpdate = new Address("AnnaNagar", "Tirupr" , 600000);
			addressService = new AddressService();
		}
		
		@Test(priority = 1, description = "Insert Address")
		public void insertAddress() throws AppException, SQLException {
			this.id = addressService.insertAddress(address1 , connection);
			Assert.assertTrue(this.id > 0);
		}
	
		@Test(priority = 2, description = "Read Address")
		public void readAddress() throws AppException, SQLException {
			System.out.println(this.id);
			address2 = addressService.readAddress(this.id, connection);
			address1.id = this.id;
			Assert.assertEquals(address2.toString(), address1.toString());
		}
		@Test(priority = 3, description = "Read All Address")
		public void readAllAddress() throws AppException, SQLException {
			Assert.assertTrue(addressService.readAllAddress(connection).toString() != null);
		}
		@Test(priority = 4, description = "Update Address")
		public void updateAddress() throws AppException, SQLException {
			addressService.updateAddress(addressUpdate, addressUpdateId, connection);
			updatedAddress = addressService.readAddress(addressUpdateId, connection);
			addressUpdate.id = addressUpdateId;
			Assert.assertEquals(updatedAddress.toString(), addressUpdate.toString());
		}
		@Test(priority = 5, description = "Delete Address")
		public void deleteAddress() throws AppException, SQLException {
			addressService.deleteAddress(this.id, connection);
		}
		
		@Test(priority = 6, description = "Delete Unused Address")
		public void deleteNotUsedAddress() throws AppException, SQLException {
			long deleteNotUsedAddress = addressService.deleteNotUsedAddress(connection);
			System.out.println(deleteNotUsedAddress);
			Assert.assertEquals(1 , deleteNotUsedAddress);
		}
		
		@AfterClass
		public void connectionClose() {
			try {
				connectionService.releaseConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
	}
*/

package com.kpr.training.jdbc.testing;

import java.util.List;
import org.testng.Assert;

import org.testng.annotations.Test;

import com.kpr.training.jdbc.connections.ConnectionPool;
import com.kpr.training.jdbc.connections.ConnectionService;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.services.AddressService;
import com.mysql.jdbc.Connection;

public class AddressServiceTest {
	
	public ConnectionPool threadOne;
	public ConnectionPool threadTwo;
	public ConnectionPool threadThree;
	
	Address addressGot;
	List<Address> addressList;
	long deleteResult;
	long createResult;
	long updateResult; 
	
	public Address address;
	public Address addressUpdate;
	
	
	public void setUp() {
		
		threadOne = new ConnectionPool();
		threadTwo = new ConnectionPool();
		threadThree = new ConnectionPool();	
		
	}
	
	@Test (priority = 1, description = "Create Address")
	public void createAddress() throws Exception {
		
		AddressService addressService = new AddressService();
		
		Connection connection = (Connection) ConnectionService.initConnection();
		
		createResult = addressService.create(address);
		
		Assert.assertTrue(createResult > 0);
		
		threadOne.remove();
	}
	
	@Test (priority = 2, description = "Update Address")
	public void updateAddress() throws Exception {
		
		AddressService addressService = new AddressService();
		
		Connection connection = threadTwo.get();
		
		updateResult = addressService.update(addressUpdate, addressUpdate.getId(), connection);
		
		Assert.assertTrue(updateResult > 0);
			
		threadTwo.remove();
	}
	
	@Test (priority = 3, description = "Read Address")
	public void readAddress() throws Exception {
		
		AddressService addressService = new AddressService();
		
		Connection connection = threadThree.get();
		
		addressGot = addressService.readAddress(42, connection);
		
		Assert.assertEquals(addressGot, address);
				
		threadThree.remove();
	}
	
	@Test (priority = 4, description = "Read All Address")
	public void readAllAddress() throws Exception{
		
		AddressService addressService = new AddressService();
		
		Connection connection = threadTwo.get();
		
		addressList = addressService.readAllAddress(connection);
		
		Assert.assertTrue(addressList != null);
		
		threadTwo.remove();
		
	}
	
	@Test (priority = 5, description = "Delete Address")
	public void deleteAddress() throws Exception {
		
		AddressService addressService = new AddressService();
		
		Connection connection = threadTwo.get();
		
		deleteResult = addressService.deleteAddress(25, connection);
		
		Assert.assertTrue(deleteResult > 0);
		
		threadTwo.remove();
		
	}
}
