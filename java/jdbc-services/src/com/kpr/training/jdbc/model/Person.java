/* 
 
Entity
   Person

Jobs To be Done:
   1.Create a class with Person variables.
   2.Create constructor to Address class for assign vlaues.
         2.1)Person.id variable and Person.created_date are an auto generated.
   3.Create getter and setter methods for private variables.

Psudeo Code:

	public class Person {
		private long id;
		private String name;
		private String email;
		private long address;
		private LocalDate birthDate;
		
	    //Getter & Setter
	}
	
 */

package com.kpr.training.jdbc.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Person {
	public long id;
	private String firstName;
	private String lastName;
	private String email;
	public long address;
	private Date birthDate;
	public Timestamp createdDate;
	
	public Person(long id, String firstName, String lastName, String email, long address, Date birthDate, Timestamp createdDate) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
		this.createdDate = createdDate;
	}
	
	public Person ( String firstName, String lastName, String email, long address, Date birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
	}
	public Person ( String firstName, String lastName, String email, Date birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
	}
	
	public long getAddress() {
		return address;
	}


	public void setAddress(long address) {
		this.address = address;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getaddress() {
		return address;
	}

	public void setaddress(long address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return new StringBuilder(" Person [id = ").append(id)
				.append("  FirstName = ").append(firstName)
				.append(", LastName = ").append(lastName)
				.append(", Email = ").append(email)
				.append(", Address = ").append(address)
				.append(", BirthDate = " ).append(birthDate)
				.append(", CreatedDate = " ).append(createdDate)
				.append("]\n")
				.toString();
				
	}	
}
