/*
    Requirement :
        write program to print fibinocci using for loop, while loop and recursion.
        
    Entities :
        FibonacciSeries
       
    Method :
        fibonacciRecursion
    
    Job to be done :
        First, get count input and then the for loop is used to loop until the limit where each iteration will call the function fibonaccinumber(int n) 
        which returns the Fibonacci number at position n.
        
        The Fibonacci function recursively calls itself adding the previous two Fibonacci numbers.
        
 */
 
package com.kpriet.java.controlflow;
    
public class FibonacciSeries { 
	public static int fibonacciRecursionFor(int n){
		if(n == 0){
			return 0;
			}
		if(n == 1 || n == 2){
			return 1;
			}
		return fibonacciRecursionFor(n-2) + fibonacciRecursionFor(n-1);
		}
	public static void main(String args[]) {
		int count = 10;
		System.out.print("Fibonacci Series of "+count+" numbers: ");
		for(int i = 0; i < count; i++) {    	
			System.out.print(fibonacciRecursionFor(i) +" ");
			}
	}
}	