/*
2.) Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

    What output do you think the code will produce if aNumber is 3?
    
    Ans:
    
       The above program produce the following output:
       
       second string
       third string

       
    Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.
*/  
 
// Program:

package com.kpriet.java.controlflow;
public class ControlFlow { 
	public static void main(String[] args) {
		int aNumber=3;
		if (aNumber >= 0) {
			if (aNumber == 0) {
				System.out.println("first string");
				}
			}
		else {
			System.out.println("second string");
			}
		System.out.println("third string");
	}
}