/*
Question :

    print the classname of all the primitive data types


*/
package com.kpriet.java.datatype;
  
public class PrimitiveType {
    public static void main(String[] args) {
        Object f = 20;
        Object g = 20.2f;
        System.out.println(f.getClass().getName()); // java.lang.Integer
        System.out.println(g.getClass().getName()); // java.lang.Float  
    }
 }
