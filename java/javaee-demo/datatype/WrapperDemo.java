package com.kpriet.java.datatype;

public class WrapperDemo {
	
    public void wrapInt(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    public void wrapChar(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    public void wrapDouble(double c) {
        System.out.println("double Value changed to Double "+c);
    }   
}
