/*
  Requirement :
  
      Write the code to print data type for the following code.
      
  Entities :
  
      DataType

  Job to be Done :

      Create the object class.
      And then print using getclass() and getName() method.     
  
*/

/*
Question :
 print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56
*/

//Code

package com.kpriet.java.datatype;

public class DataType {
    public static void main(String[] args) {
    	Object a = 123 / 2 ; 
	    Object b = 100 / 24 ;
	    Object c = 100.10 / 10;
	    Object d = 'Z' / 2 ;
	    Object e = 10.5 / 0.5 ;
	    Object f =  12.4 % 5.5;
	    Object g = 100 % 56;
	    System.out.println(a.getClass().getName());
	    System.out.println(b.getClass().getName());
	    System.out.println(c.getClass().getName());
	    System.out.println(d.getClass().getName());
	    System.out.println(e.getClass().getName());
	    System.out.println(f.getClass().getName());
	    System.out.println(g.getClass().getName());
    }
}
