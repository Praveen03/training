/*
    Requirement:          
          Demonstrate overloading with Wrapper types
          
    Entity:
         WrapperDemo
         WrapperOverload
       
    Method signature :
        public void wrapInt(int a)
        public void wrapChar(char b)
        public void wrapDouble(double c)
        
    Job to be done :
      Create the class WrapperDemo and then declare the method with in the class.
      And the create another one class WrapperOverload in that create class object for the WrapperDemo.
      Using the object overloading with Wrapper types.

*/         
package com.kpriet.java.datatype;

public class WrapperOverload extends WrapperDemo {
    
    public static void main(String[] args) {
        
        Integer integer = 12;
        Character chara = 'W';
        Double doub = 22.31;
        WrapperDemo wrap = new WrapperDemo();
        wrap.wrapInt(integer);
        wrap.wrapChar(chara);
        wrap.wrapDouble(doub);
        
    }
}