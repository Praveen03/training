
package com.kpriet.java.inheritance;

public class Snake extends Animal {
    
       public void action() {
          System.out.println("Hiss.....");
       }
       
       public static void main(String args[]){
          Snake snake = new Snake();
          snake.action();
    }
}