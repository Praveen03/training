1.) Consider the following class:

    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

 - What are the class variables?

    Class variables also known as static variables are declared with the static keyword in a class, but outside a method, constructor or a block.
    
 	In the above example, (public static int x = 7) is a static variable.

 - What are the instance variables?
       
    Instance variables are declared in a class, but outside a method.
    
    No, instance variable present in the given class.
