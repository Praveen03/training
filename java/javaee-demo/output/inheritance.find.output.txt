
 Consider the following two classes:
 
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    
    
    a. Which method overrides a method in the superclass?
    
             methodTwo will overrides a method in the superclass.
    
    b. Which method hides a method in the superclass?
    
             methodFour will hides a method in the superclass.
    
    c. What do the other methods do?
            
            Other methods shows compile time error.