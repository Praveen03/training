
/*
Compare the enum values using equal method and == operator.

  ->  == operator never throws NullPointerException.
  ->  equals() method can throw NullPointerException.
  
*/   

// Example :
   
package com.kpriet.java.enumeration;
public class Enum { 
	private static Dept dept;
	public enum Dept { CSE, ECE, CIVIL, MECH } 
	public static void main(String[] args) { 
		dept = null;  
        System.out.println(dept == Dept.CSE); // false
        System.out.println(dept.equals(Dept.CSE)); // java.lang.NullPointerException
     } 
}
    