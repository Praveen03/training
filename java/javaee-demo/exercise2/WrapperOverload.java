/*
    Requirement:          
          Demonstrate overloading with Wrapper types
          
    Entity:
         WrapperDemo
         WrapperOverload
       
    Method signature :
        public void wrapInt(int a)
        public void wrapChar(char b)
        public void wrapDouble(double c)
        
    Job to be done :
      Create the class WrapperDemo and then declare the method with in the class.
      And the create another one class WrapperOverload in that create class object for the WrapperDemo.
      Using the object overloading with Wrapper types.

*/         

public class WrapperDemo {

    public void wrapInt(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    
    public void wrapChar(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    
    public void wrapDouble(double c) {
        System.out.println("double Value changed to Double "+c);
    }
    
}

public class WrapperOverload {
    
    public static void main(String[] args) {
        
        Integer integer = 12;
        Character chara = 'W';
        Double doub = 22.31;
        WrapperDemo wrap = new WrapperDemo();
        wrap.wrapInt(integer);
        wrap.wrapChar(chara);
        wrap.wrapDouble(doub);
        
    }
}