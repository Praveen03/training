package com.hatchmind.training.java.calc;

import com.hatchmind.training.java.shape;

 public class Calculate extends Shape { 
 
      public static void main(String args[])
        {
            Calculate calc = new Calculate();
            calc.areaCircle(12);
            calc.areaSquare(6);
            calc.periCircle(3);
            calc.periSquare(5);
            
        }
  }