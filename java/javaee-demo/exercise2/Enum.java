
/*
Compare the enum values using equal method and == operator.

  ->  == operator never throws NullPointerException.
  ->  equals() method can throw NullPointerException.
  
*/   

// Example :
   
    package com.hatchmind.training.java.core;

    public class Enum { 
           public enum Dept { CSE, ECE, CIVIL, MECH } 
        public static void main(String[] args) 
        { 
            Dept dept = null;  
            System.out.println(dept == Dept.CSE); // false
            System.out.println(dept.equals(Dept.CSE)); // java.lang.NullPointerException
        } 
    }
    