
/*

  Question :
        Write a program that computes your initials from your full name and displays them.
      
  Requirement :
        To write a program that computes your initials from your full name and displays them.
  
  Enities :
        StringInitial
  
  Job to be Done :
       Print the of initial of full name.

*/

package com.hatchmind.training.java.strings;

public class StringInitial {
    public static void main(String[] args) {
        String myName = " Jorge D. Danial";
                        
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
        
            if (Character.isUpperCase(myName.charAt(i))) {
            
                myInitials.append(myName.charAt(i));
                
            }
        }
        System.out.println("My Name is: " + myName);
        System.out.println("My initials are: " + myInitials);
    }
}