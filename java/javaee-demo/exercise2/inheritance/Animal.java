/*
 Question :
          Demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

  Requirement :
         Write a code to show inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects.
         
   Entities :
         Animal
         Dog
         Cat
         Snake
    
    Method Signature :
         public void action()
         public void bark()
         public void meow()
         public void hiss()
         
    Job To be Done :
       In Animl class, Creating class instance and method.
       Using action method overloading it.
       In Dog class, class instance and method.
       Using action method overriding it, and similer to Cat and Snake the class.      
        
*/

package com.hatchmind.training.java.inanimal;
 
public class Animal {
    
       String name;
       String color;
       
      public void action(String name, String color){
         System.out.println("Moving....." + name + color);
      }
      
      public void action(String name){
          System.out.println("Moving....." + name);
      } 
      
    public static void main(String args[]){
        Animal animal = new Animal();
        animal.action("Dog","black");
        animal.action("Dog");
    }
}