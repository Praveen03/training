/* 
    Requirement :
         Find wrong with the following program.
        
    Entites :
         Rectangle
    
    Method :
         area()
   
   Job to be done :
         Write the program correctly.
             
Question :

 What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect ;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
       
Answer :
      
    - class name is wrong.
    - creating object for Rectangle is wrong.
    - Function area() is missing.
    
*/
 
 // CODE
  
   package com.hatchmind.training.java.core;

   public class Rectangle {
        int width;
        int height;
        public int area() {
            int area;
            area = width*height;
            return area;
        }    
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
        }
    }
 