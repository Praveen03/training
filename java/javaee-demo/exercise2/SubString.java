/*

 Question : 
      How long is the string returned by the following expression? What is the string?
      "Was it a car or a cat I saw?".substring(9, 12)

 Requirement :
       Write the program to string returned by the following expression.
 
 Entities :
       SubString 
 
*/
package com.hatchmind.training.java.substring;

public class SubString {

   public static void main(String args[]){
      
      String str = "Was it a car or a cat I saw?";
      System.out.println(str.substring(9, 12));
      
   }
}


/*
Answer :

The length of substring is 3, It does not include the space and the output is car.

*/