/*
    Question : 
           
            Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
            For example, suppose that you enter the following: java Adder 1 3 2 10
            The program should display 16 and then exit. 
            The program should display an error message if the user enters only one argument.
               
    Requirements : 
          
            To create a program that reads integer arguments.
            To display the sum of the integer arguments from the command line.
            
    Entities :             
           IntegerAdder 
  
    Jobs to be done : 
    
         Check the argument lenth if less than 2 print enter two command-line arguments.
         Whether length is greater than two, using the for loop sum integer argument and print it.
*/

package com.hatchmind.training.java.intadder;

public class IntegerAdder {

    public static void main(String[] args) {
        
        int numArgs = args.length;
    
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } 
        else {
        int sum = 0;   
        for (int i = 0; i < numArgs; i++) {
            sum += Integer.valueOf(args[i]).intValue();
        }
            System.out.println(sum);
        }
    }
}