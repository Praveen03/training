/*
    Question :
    
        Program to store and retrieve four boolean flags using a single int


    1.Requirement :
        To write a program store and retrieve four boolean flags using a single int.
        
    2.Entity :
        BooleanFlag
        
    3.Function Declaration :
         public static void main(String[] args)
         
    4.Jobs to be done :
    
        Create a BooleanFlag class.
        Initialise the value for int variable.
        Using the conditional operator to check the codition if true, prints the animal name
        and condition is false prints the variable name not found.
        
*/ 

public class BooleanFlag {

    public static void main(String[] args) {

        int dog = 2;
        int cow = 4;
        int cat = 0;
        int snake = 0;
        int animal;
        animal = dog | cat | snake;
        System.out.println("Looking for an animal...");
        System.out.println((animal & dog) > 0 ? "--> Dog " : "NO Dog");
        System.out.println((animal & cow) > 0 ? "--> cow" : "NO cow");
        System.out.println((animal & cat) > 0 ? "--> cat" : "NO cat");
        System.out.println((animal & snake) > 0 ? "--> snake" : "NO snake");
    }

}