/*
     Question :
     
         Demonstrate overloading with varArgs.
     
     Requirement : 
     
          To write program overloading with varArgs.
           
     Entites :
        
          VarargsDemo
     
     Method Signature :
         
        public static void Varargs(int... args)  
        public static void Varargs(char... args)        
        public static void Varargs(double... args)
     
     Jobs to be done :
         
        Calling display() method with differnt types of parameter.
        A method that takes varargs and printed using the for loop.
              
*/    

package com.kpriet.java.varargs;
  
public class Varargs { 
	public static void display(int num, String... values){ 
		System.out.println("Number is "+num);
		for(String s:values) {  
			System.out.println(s);  
			}  
		System.out.println(); 
	} 
	public static void display(boolean ... a) { 
		System.out.print("Number of boolean: " + a.length + " Contents: ");
        for(boolean x : a) 
           System.out.print(x + " "); 
        System.out.println(); 
    }     
    public static void main(String args[]){ 
    	display(500,"hello");  
        display(1000,"my","name","is","varargs");
        display(true, false, false);
    }   
}  