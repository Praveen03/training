/*  

 Question :

     Demonstrate abstract classes using Shape class.
        Shape class should have methods to calculate area and perimeter
        Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively

  Requirement : 
     
        To write a program for abstract class using shape class.
           
  Entites :
        
        Shape
        Calculate
     
   Method Signature :
   
         public void areaSquare(double a)
         public void areaCircle(double r)
         public void periSquare(double a)
         public void periCircle(double r)
     
   Jobs to be done :
         
         Create the abstract class and method for calculating area and permeter for square and circle.
         And create the another java file Calculate to call the methods in the Shape.
        
*/
package com.kpriet.java.abstraction;
 
public abstract class Circle {    
 
       public void areaSquare(double a) {
            double area = a*a;
            System.out.println("Area of Square: "+area);
        }
         
       public void areaCircle(double r) {
            double area = 3.14*r*r;
            System.out.println("Area of Circle: "+area);
        }


        public void periSquare(double a) {
             double perimeter=4*a;
            System.out.println("Perimeter of Square: "+perimeter);
        }
         
       public void periCircle(double r) {
            double area = 2 * 3.14 * r ;
            System.out.println("Perimeter of Circle: "+area);
        }
}
   