package com.hatchmind.training.java.core;

import com.hatchmind.training.java.Driver;
import com.hatchmind.training.java.Wheel;

public class Car extends Driver {
    
    public String name;
    public String color;
    public int modelNumber;
    
    // Creating Constructor
    public Car(String name, String color, int modelNumber) {
        this.name = name;
        this.color = color;
        this.modelNumber =modelNumber;
    }
    
    private Car(String name, String color) {
        System.out.println("Car Name : "+name+"\nCar Color :"+color+"\n");
    }
    
    // Non-Parameterized Constructor
    public Car() {
        System.out.println("Car Color :"+color+"\n"); 
    }
    // Method
    protected void printCarDetails() {
        System.out.println("\nCar Name : "+name+"\nCar Color :"+color+"\nCar ModelNumber :"+modelNumber+"\n");
    }
    
    {
        color = "blue";
    }
    
    public static void main(String args[]) {
        Car car = new Car("Honda", "White", 2015);
        car.printCarDetails();
        Car car1 = new Car("Ford","Black");
        car1.printCarDetails();
        Car car2 = new Car();
        Car car3 = new Car("Hundai", "Red");
        Wheel wheel = new Wheel();
        wheel.run();                       // result compile time error - private access for wheel
        Driver driver = new Driver();
        driver.drive();                  // result compile time error - protected access for Driver
        car.drive();
    }  
}
