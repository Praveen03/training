/*
    Requirements : 
     
           To create a program to read floating point arguments.
           To display the sum of arguments using exactly two digits to the right of the decimal point.
                   
     Entities : 
           FloatingPointAdder   
     
     Jobs to be done : 
     
         Check the argument lenth if less than 2 print enter two command-line arguments.
         Whether length is greater than two, using the for loop sum floating argument and display it.  
*/

package com.kpriet.java.core;

import java.text.DecimalFormat;

public class FloatingPointAdder {
    
    public static void main(String[] args) {
    	if (args.length < 2) {
    		System.out.println("Enter two command-line arguments.");
    		} 
    	else {
    		double sum = 0.0;
    		for (int i = 0; i < args.length; i++) {
        	   sum += Double.valueOf(args[i]).doubleValue();
        	   }
    		DecimalFormat myFormatter = new DecimalFormat();
            String display = myFormatter.format(sum);
            System.out.println(display);
        }
    }
}
