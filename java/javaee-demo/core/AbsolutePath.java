/*
Question :

   Print the absolute path of the .class file of the current class

    Requirement : 
     
         write the program to print the absolute path of the .class file of the current class.
           
     Entites :
        
          AbsolutePath
     
     Jobs to be done :
         
       Using the System.getProperty() to get the absolutepath of the class.
        
*/

package com.kpriet.java.core;

public class AbsolutePath {
    
    public static void main(final String[] args)
    {
        String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
    }
} 

