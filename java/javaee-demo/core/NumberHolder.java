/*
Requirement :
        write some code that creates an instance of the class and initializes its two member variables with provided values,
        and then displays the value of each member variable.
    
    Entities :
        NumberHolder
*/

/* 
Question 

 Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
      public static void main(String[] args) {
        NumberHolder holder = new NewmberHolder();
        holder.anInt = 12345;
        holder.aFloat = 25.6;
      }
    }
    
*/
    
    
// Code 

package com.kpriet.java.core;

public class NumberHolder {
	
	public static int anInt;
	public static Float aFloat;
	
	public static void main(String[] args) {
		
        NumberHolder.anInt = 12345;
        NumberHolder.aFloat = 25.6f;
        System.out.println(anInt);
        System.out.println(aFloat);
        
    }
}
