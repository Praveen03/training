/*   
 Question :
 
    sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }


  Requirement : 
     
          To write a program sort and print following String[] alphabetically.
           
     Entites :
        
          StringSort
     
     Jobs to be done :
         
         Enter the string to sort and print.
         import array libray and using the build in function Array.sort() to sort the following string.
             
*/

 package com.kpriet.java.string;

 import java.util.Arrays;

 public class StringSort {
    public static void main(String[] args) {
       String[] district = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
       System.out.println("Before sorting "+Arrays.toString(district));
       Arrays.sort(district);
       System.out.println("After sorting "+Arrays.toString(district));
    }
 }