/*
 Question :

   Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";
    
 Requirement :
    Write program to concatenate string using two ways.
   
 Entites :
    ConcatString
    
 Job to be Done :
    concatenate string using concat() method and + .

*/

package com.kpriet.java.string;  
public class ConcatString {
	public static void main(String args[]){    
		String hi = "Hi, ";
        String mom = "mom.";
        System.out.println(hi.concat(mom));
        System.out.println(hi + mom);
	}
}