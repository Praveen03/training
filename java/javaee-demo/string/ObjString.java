/*

Question :

    Demonstrate object equality using Object.equals() vs ==, using String objects

 
Answer :

     Main difference between .equals() method and == operator is that one is method and other is operator.
     We can use == operators for reference comparison (address comparison).
     And .equals() method for content comparison.
     In simple words, == checks if both objects point to the same memory location.
     Whereas .equals() evaluates to the comparison of values in the objects.

*/
     
// Example :     
package com.kpriet.java.string;
 
public class ObjString {
    
    public static void main(final String[] args)
    {
             
    Object obj1 = new Object();
    Object obj2 = new Object();
         
    // == should return false
    System.out.println("Comparing two different Objects with == operator: " + obj1==obj2);
         
    //equals should return false because obj1 and obj2 are different
    System.out.println("Comparing two different Objects with equals() method: " + obj1.equals(obj2));
  
    }
} 
